The First International Joint Conference on Autonomous Agents & Multiagent Systems, AAMAS 2002, July 15-19, 2002, Bologna, Italy, Proceedings.
Victor R. Lesser
Thomas Juan
Adrian R. Pearce
Leon Sterling
Toshiaki Arai
Frieder Stolzenburg
Steven Shapiro
Yves Lespérance
Hector J. Levesque
Philippe Massonet
Yves Deville
Cédric Nève
Fausto Giunchiglia
John Mylopoulos
Anna Perini
Lin Padgham
Michael Winikoff
Arnon Sturm
Onn Shehory
Clinton Heinze
Leon Sterling
Guoqiang Zhong
Kenichi Takahashi
Satoshi Amamiya
Tsunenori Mine
Makoto Amamiya
Clemens Fritschi
Klaus Dorer
Gilles Klein
Amal El Fallah-Seghrouchni
Patrick Taillibert
Tadachika Ozono
Shoji Goto
Nobuhiro Fujimaki
Toramatsu Shintani
Jan Murray
Alexander Babanov
John Collins
Maria L. Gini
Takayuki Ito
Makoto Yokoo
Shigeo Matsubara
Tuomas Sandholm
Subhash Suri
Andrew Gilpin
David Levine
Esther David
Rina Azoulay-Schwartz
Sarit Kraus
Steven P. Fonseca
Martin L. Griss
Reed Letsinger
Martin K. Purvis
Mariusz Nowostawski
Stephen Cranefield
K. Suzanne Barber
Dung N. Lam
Konstantinos Prouskas
Jeremy Pitt
Manuel Kolp
Paolo Giorgini
John Mylopoulos
Christopher H. Brooks
Edmund H. Durfee
Yuko Sakurai
Makoto Yokoo
Makoto Yokoo
Koutarou Suzuki
Cuihong Li
Katia P. Sycara
Cao Da-Jun
Xu Liang-Xian
Kate Larson
Tuomas Sandholm
S. Shaheen Fatima
Michael Wooldridge
Nicholas R. Jennings
Elth Ogston
Stamatis Vassiliadis
Kate Larson
Tuomas Sandholm
Paul S. A. Reitsma
Peter Stone
János A. Csirik
Michael L. Littman
Kidane Asrat Ghebreamiak
Arne Andersson
Pu Huang
Alan Scheller-Wolf
Katia P. Sycara
Wolfram Conen
Tuomas Sandholm
Pinata Winoto
Tiffany Ya Tang
Wei Chen
Keith S. Decker
Antonella Di Stefano
Corrado Santoro
Praveen Paruchuri
Alok Reddy Pullalarevu
Kamalakar Karlapalem
Lamjed Ben Said
Thierry Bouron
Alexis Drogoul
Jan M. Allbeck
Karin Kipper
Charles Adams
William Schuler
Elena Zoubanova
Norman I. Badler
Martha Stone Palmer
Aravind K. Joshi
David C. Brogan
Jessica K. Hodgins
Julia V. Frolova
Victor V. Korobitsin
A. Zhang
M. Chung
B. Lee
R. Cho
Sanza T. Kazadi
R. Vishwanath
Marjorie Le Bars
Jean-Marie Attonaty
Suzanne Pinson
Robert A. Ghanea-Hercock
Francesco Amigoni
Nicola Gatti
Marco Somalvico
Franziska Klügl
Ana L. C. Bazzan
Kian Hsiang Low
Wee Kheng Leow
Marcelo H. Ang
Monica N. Nicolescu
Maja J. Mataric
Bruno S. Pimentel
Guilherme A. S. Pereira
Mario Fernando Montenegro Campos
Ariel Felner
Roni Stern
Sarit Kraus
H. Van Dyke Parunak
Sven Brueckner
John A. Sauter
Robert Savit
Kiam Tian Seow
Khee Yin How
Min-Jung Yoo
Martin J. Kollingbaum
Timothy J. Norman
Lik Mui
Mojdeh Mohtashemi
Ari Halberstadt
Sandip Sen
Neelima Sajja
Bin Yu
Munindar P. Singh
Konstantinos Karasavvas
Richard A. Baldock
Albert Burger
Miquel Montaner
Beatriz López
Josep Lluís de la Rosa
Silvia Breban
Julita Vassileva
Xiao Feng Wang
Kartik Hosanagar
Ramayya Krishnan
Pradeep K. Khosla
Sviatoslav Braynov
Tuomas Sandholm
Partha Sarathi Dutta
Sandip Sen
Von-Wun Soo
Chun-An Hung
Ana L. C. Bazzan
Vanessa Lindemann
Victor R. Lesser
Marc Cavazza
Fred Charles
Steven J. Mead
Robert C. Burke
Bruce Blumberg
Stacy Marsella
Jonathan Gratch
Patrick Doyle
Helmut Prendinger
Mitsuru Ishizuka
Eric Chown
Randolph M. Jones
Amy E. Henninger
Pilar Herrero
Angélica de Antonio
Steve Benford
Chris Greenhalgh
Sumedha Kshirsagar
Nadia Magnenat-Thalmann
Matthew D. Schmill
Paul R. Cohen
Piotr J. Gmytrasiewicz
Christine L. Lisetti
Jun Wang
Les Gasser
Nobuo Suematsu
Akira Hayashi
Kagan Tumer
Adrian K. Agogino
David Wolpert
Santiago Ontañón
Enric Plaza
Simon Parsons
Michael Wooldridge
Leila Amgoud
Peter McBurney
Simon Parsons
Michael Wooldridge
Luís Brito
José Neves
Radhika Nagpal
Christine Bourjot
Vincent Chevrier
Vincent Thomas
John A. Sauter
Robert S. Matthews
H. Van Dyke Parunak
Sven Brueckner
David Servat
Alexis Drogoul
H. Van Dyke Parunak
Sven Brueckner
John A. Sauter
Philippe Mathieu
Jean-Christophe Routier
Yann Secq
José M. Vidal
Franco Zambonelli
H. Van Dyke Parunak
Jiming Liu
Shiwu Zhang
Yiming Ye
Marius-Calin Silaghi
Boi Faltings
Leonid Sheremetov
José C. Romero Cortés
Esteve del Acebo
Josep Lluís de la Rosa
Catholijn M. Jonker
Jacky L. Snoep
Jan Treur
Hans V. Westerhoff
Wouter C. A. Wijngaards
Josep M. Pujol
Ramon Sangüesa
Jordi Delgado
Jordi Sabater
Carles Sierra
Jaime Simão Sichman
Rosaria Conte
Timothy J. Norman
Chris Reed
François Legras
Jomi Fred Hübner
Jaime Simão Sichman
Olivier Boissier
Xiaoqin Zhang
Victor R. Lesser
Thomas Wagner
Martin D. Beer
Janice Whatley
Sehl Mellouli
Guy W. Mineau
Daniel Pascot
Hamza Mazouzi
Amal El Fallah-Seghrouchni
Serge Haddad
Pinar Yolum
Munindar P. Singh
Nicoletta Fornara
Marco Colombetti
Sanjeev Kumar
Marcus J. Huber
Philip R. Cohen
Andreas Gerber
Nils Kammenhuber
Matthias Klusch
Stephen Cranefield
Mariusz Nowostawski
Martin K. Purvis
Juan Manuel Serrano
Sascha Ossowski
Frank Guerin
Jeremy Pitt
Piotr J. Gmytrasiewicz
Terry R. Payne
Massimo Paolucci
Rahul Singh
Katia P. Sycara
Floris Wiesman
Nico Roos
Paul Vogt
Sasu Tarkoma
Mikko Laukkanen
Sanjeev Kumar
Philip R. Cohen
Marcus J. Huber
Nicola Muscettola
Steve A. Chien
Rob Sherwood
Gregg Rabideau
Rebecca Castaño
Ashley Davies
Michael C. Burl
Russell Knight
Timothy M. Stough
Joseph Roden
Paul Zetocha
Ross Wainwright
Pete Klupar
Jim Van Gaasbeck
Pat Cappelaere
Dean Oswald
Karen Zita Haigh
John Phelps
Christopher W. Geib
António Lopes
Sérgio Gaio
Luís Miguel Botelho
Antonio Moreno
David Isern
Gerald Tesauro
Jonathan Bredin
Michael Schillo
Christian Kray
Klaus Fischer
Partha Sarathi Dutta
Sandip Sen
Andrew Byde
Chris Preist
Nicholas R. Jennings
Hiroshi Ishiguro
Cefn Hoile
Fang Wang
Erwin Bonsma
Paul Marrow
Subrata Kumar Das
Kurt Shuster
Curt Wu
Tieyan Li
Kwok-Yan Lam
Jonathan Carter
Ali A. Ghorbani
Stephen Marsh
Tieyan Li
Kwok-Yan Lam
Xiaocong Fan
Zeljko Obrenovic
Dusan Starcevic
Emil Jovanov
Vlada Radivojevic
Mark Klein
Chrysanthos Dellarocas
Juan A. Rodríguez-Aguilar
Armin Stranjak
Igor Cavrak
Damir Kovacic
Mario Zagar
T. D. Lowen
Gregory M. P. O'Hare
P. T. O'Hare
Zakaria Maamar
Wathiq Mansoor
Qusay H. Mahmoud
Arran Bartish
Charles Thevathayan
Laurent Vercouter
Zahia Guessoum
Jean-Pierre Briot
S. Charpentier
Olivier Marin
Pierre Sens
Fabiola López y López
Michael Luck
Mark d'Inverno
Michael Rovatsos
Gerhard Weiß
Marco Wolf
Trevor J. M. Bench-Capon
Paul E. Dunne
Martin Fredriksson
Rune Gustavsson
Virginia Dignum
John-Jules Ch. Meyer
Hans Weigand
Wamberto Weber Vasconcelos
Jordi Sabater
Carles Sierra
Joaquim Querol
Holger Knublauch
Mirko Viroli
Andrea Omicini
David Kinny
Gal A. Kaminka
Michael H. Bowling
Alan Fedoruk
Ralph Deters
Les Gasser
Kelvin Kakugawa
Mark Klein
Peyman Faratin
Hiroki Sayama
Yaneer Bar-Yam
Catherine Pelachaud
Valeria Carofiglio
Berardina De Carolis
Fiorella de Rosis
Isabella Poggi
David R. Traum
Jeff Rickel
James F. Allen
Nate Blaylock
George Ferguson
Charles Rich
Neal Lesh
Andrew Garland
Jeff Rickel
Yasuhiko Kitamura
Hideki Tsujimoto
Teruhiro Yamada
Taizo Yamamoto
Taras Mahlin
Jeffrey S. Rosenschein
Claudia V. Goldman
Yanguo Jing
Keith E. Brown
Nick K. Taylor
Marcello L'Abbate
Ulrich Thiel
Nikos I. Karacapilidis
Pavlos Moraitis
Alexander Huber
Bernd Ludwig
Terry R. Payne
Rahul Singh
Katia P. Sycara
Michael Beetz
Sebastian Buck
Robert Hanek
Thorsten Schmitt
Bernd Radig
Kasper Støy
Wei-Min Shen
Peter M. Will
Randall W. Hill Jr.
Youngjun Kim
Jonathan Gratch
Norimichi Ukita
Takashi Matsuyama
Hüseyin Sevay
Costas Tsatsoulis
Dídac Busquets
Ramon López de Mántaras
Carles Sierra
Bruno Scherrer
François Charpillet
Mohammad Ghavamzadeh
Sridhar Mahadevan
Hongchi Shi
Spyridon Revithis
Su-Shing Chen
Michael Fisher
Chiara Ghidini
Paul Scerri
David V. Pynadath
Milind Tambe
Catholijn M. Jonker
Jan Treur
David V. Pynadath
Milind Tambe
Natasha Alechina
Brian Logan
Jan-Willem Roorda
Wiebe van der Hoek
John-Jules Ch. Meyer
Ken Satoh
Keiji Yamamoto
Robert Demolombe
Erwan Hamon
Jürgen Dix
Sarit Kraus
V. S. Subrahmanian
Andreas Herzig
Dominique Longin
Naoyuki Nide
Shiro Takata
Eudenia Xavier Meneses
Flávio S. Corrêa da Silva
James Harland
Michael Winikoff
Anna Ciampolini
Paola Mello
Paolo Torroni
Evelina Lamma
Michael Schroeder
Ralf Schweimeier
Jürgen Dix
Héctor Muñoz-Avila
Dana S. Nau
Lingling Zhang
Alessio Lomuscio
Marek J. Sergot
Luis Carlos de Sousa Menezes
Geber Ramalho
Hermano Perrelli de Moura
László Aszalós
Andreas Herzig
Michael Wooldridge
Michael Fisher
Marc-Philippe Huget
Simon Parsons
David Poutakidis
Lin Padgham
Michael Winikoff
Christos Georgousopoulos
Omer F. Rana
Paul E. Dunne
Michael Wooldridge
Michael Laurence
Mark Iwen
Amol Dattatraya Mali
Nico Roos
Annette ten Teije
André Bos
Cees Witteveen
Weixiong Zhang
Zhidong Deng
Guandong Wang
Lars Wittenburg
Zhao Xing
Andrew B. Williams
Todd A. Krygowski
George Thomas
Tarek Helmy
Satoshi Amamiya
Makoto Amamiya
Filippo Menczer
W. Nick Street
Narayan Vishwakarma
Alvaro E. Monge
Markus Jakobsson
Panos M. Markopoulos
Jeffrey O. Kephart
Michael N. Huhns
Larry M. Stephens
Nenad Ivezic
Fabien Gandon
Laurent Berthelot
Rose Dieng-Kuntz
Wei Yang
Katia P. Sycara
Onn Shehory
Maria Goldstein
Adi Shulman
Arnon Sturm
Boris Yurovitsky
Yun Peng
Youyong Zou
Xiaocheng Luan
Nenad Ivezic
Michael Grüninger
Albert T. Jones
Federico Bergenti
Agostino Poggi
Giovanni Rimassa
Paola Turci
Karen L. Myers
David N. Morley
Katia P. Sycara
Marc Esteva
David de la Cruz
Carles Sierra
Alexander Artikis
Jeremy Pitt
Marek J. Sergot
Leen-Kiat Soh
Costas Tsatsoulis
Alessandro Ricci
Andrea Omicini
Enrico Denti
David Wolpert
John W. Lawson
Philippe Caillou
Samir Aknine
Suzanne Pinson
Robert L. Axtell
William Agassounon
Alcherio Martinoli
Ping Xuan
Victor R. Lesser
Cora B. Excelente-Toledo
Nicholas R. Jennings
Sandip Sen
Partha Sarathi Dutta
Sanmay Das
Barbara J. Grosz
Avi Pfeffer
Joao Luis T. da Silva
Yves Demazeau
Rushed Kanawati
Maria Malek
Timothy W. Rauenbusch
Barbara Dunin-Keplicz
Rineke Verbrugge
Andrea Bonarini
Marcello Restelli
Patrick Riley
Manuela M. Veloso
Gal A. Kaminka
Osher Yadgar
Sarit Kraus
Charles L. Ortiz Jr.
John R. Rose
Michael N. Huhns
Soumik Sinha Roy
William H. Turkett Jr.
Marco Colombetti
Mario Verdicchio
Guido Boella
Wiebe van der Hoek
Michael Wooldridge
Yiming Ye
Stephen J. Boies
Jiming Liu
Xun Yi
Stefania Bandini
Sara Manzoni
Carla Simone
Pedro V. Sander
Denis Peleshchuk
Barbara J. Grosz
Katsutoshi Hirayama
Makoto Yokoo
Xiaoqin Zhang
Victor R. Lesser
Charles L. Ortiz Jr.
Eric Hsu
Paolo Torroni
Bruce Edmonds
Carlos H. C. Ribeiro
Renê Pegoraro
Anna Helena Reali Costa
Jörg Denzinger
Sean Ennis
Tang Chao
Feng Shan
Simon X. Yang
Steven J. Lynden
Omer F. Rana
Koichi Moriyama
Masayuki Numao
Olivier Buffet
Alain Dutech
François Charpillet
Takahiro Uchiya
Takuo Suganuma
Tetsuo Kinoshita
Norio Shiratori
Bin Yu
Munindar P. Singh
Carles Sierra
Jordi Sabater
Jaume Agustí-Cullell
Pere Garcia
Ascensión Gallardo-Antolín
Ángel Navia-Vázquez
Harold Y. Molina-Bulla
A. B. Rodríguez-González
Francisco J. Valverde-Albacete
Jesús Cid-Sueiro
Aníbal R. Figueiras-Vidal
T. Koutris
C. Xirouhaki
Manolis Koubarakis
Paolo Busetta
Antonia Donà
Michele Nori
Loris Penserini
Maurizio Panti
Luca Spalazzi
Itai Yarom
Jeffrey S. Rosenschein
Claudia V. Goldman
Penny Noy
Michael Schroeder
Rafael H. Bordini
Ana L. C. Bazzan
Rafael de Oliveira Jannone
Daniel M. Basso
Rosa Maria Vicari
Victor R. Lesser
Hosam Hanna
Abdel-Illah Mouaddib
Fabrice Lamarche
Stéphane Donikian
Peyman Faratin
Bartel Van de Walle
Dmitri A. Dolgov
Edmund H. Durfee
Fang Wang
Zili Zhang
Chengqi Zhang
Mikko Laukkanen
Heikki Helin
Heimo Laamanen
Damian A. Isla
Bruce Blumberg
Stefan Fischer
Werner Kießling
Stefan Holland
Michael Fleder
Richard Angros
W. Lewis Johnson
Jeff Rickel
Andrew Scholer
Kazuhiko Kawamura
Tamara E. Rogers
Xinyu Ao
Toru Takahashi
Hideaki Takeda
Randall W. Hill Jr.
Changhee Han
Michael van Lent
David C. Brogan
Yannick Loitière
Paul E. Rybski
Maria L. Gini
Dean F. Hougen
Sascha Stoeter
Nikolaos Papanikolopoulos
Itsuki Noda
Markus J. Kaiser
Kwok Ching Tsui
Jiming Liu
Krunoslav Trzec
Darko Huljenic
Petra Funk
Ingo Zinnikus
Romaric Charton
Anne Boyer
François Charpillet
M. Mostagir
Keith Decker
Jerzy W. Bala
Sung Baik
Ali Hadjarian
B. K. Gogia
Chris Manthorne
