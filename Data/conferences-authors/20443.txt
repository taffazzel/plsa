ACM/IEEE Joint Conference on Digital Libraries, JCDL 2001, Roanoke, Virginia, USA, June 24-28, 2001, Proceedings.
Andreas Rauber
Alexander Müller-Kögler
Paolo Frasconi
Giovanni Soda
Alessandro Vullo
James W. Warner
Elizabeth W. Brown
Kin Hui
Wai Lam
Helen M. Meng
David Yaron
D. Jeff Milton
Rebecca Freeland
Jean R. Laleuf
Anne Morgan Spalter
Catherine C. Marshall
Morgan N. Price
Gene Golovchinsky
Bill N. Schilit
James B. Lloyd
Timothy W. Cole
Donald Waters
Caroline Arms
Simeon Warner
Jeffrey Young
William E. Moen
Ray R. Larson
Carl Lagoze
Herbert Van de Sompel
Hussein Suleman
Xiaoming Liu
Kurt Maly
Mohammad Zubair
Michael L. Nelson
Luis Francisco-Revilla
Frank M. Shipman III
Richard Furuta
Unmil Karadkar
Avital Arora
Greg Keast
Elaine G. Toms
Joan M. Cherry
Michael Chau
Daniel Dajun Zeng
Hsinchun Chen
Robin D. Burke
Ching Chen
Wen Gao
Hsueh-hua Chen
Li-Zhu Zhou
Von-Wun Soo
Ian H. Witten
David Bainbridge
Stefan J. Boddie
Bruce Rosenstock
Michael Gertz
Brad A. Myers
Juan P. Casares
Scott M. Stevens
Laura Dabbish
Dan Yocum
Albert T. Corbett
Jia-Yu Pan
Christos Faloutsos
Terence R. Smith
Greg Janee
James Frew
Anita Coleman
Anne J. Gilliland-Swetland
Gregory H. Leazer
John MacColl
Joe Futrelle
Su-Shing Chen
Kevin Chen-Chuan Chang
Nina Wacholder
David Kirk Evans
Judith Klavans
David Fulker
Sharon S. Dawes
Leonid A. Kalinichenko
Tamara Sumner
Costantino Thanos
Alex Ushakov
Karen E. Pettigrew
Joan C. Durrance
Peter Brophy
Shelagh Fisher
Lynne Davis
Melissa Dawe
Steve Jones
Gordon W. Paynter
Michael Khoo
Adrienne Muir
G. Sayeed Choudhury
Mark Lorie
Erin Fitzpatrick
Benjamin Hobbs
Gregory S. Chirikjian
Allison M. Okamura
Nicholas E. Flores
Nadia Caidi
Alan S. Inouye
Margaret Hedstrom
Dale Flecker
David M. Levy
Ann Blandford
Hanna Stelmaszewska
Nick Bryan-Kinns
Sally Jo Cunningham
Chris Knowles
Nina Reeves
Gary Geisler
David McArthur
Sarah Giersch
Judith Klavans
Smaranda Muresan
Micah Altman
Leonid Andreev
Mark Diggory
Gary King
Elizabeth Kolster
A. Sone
Sidney Verba
Daniel L. Kiskis
Michael Krot
Bruce R. Barkstrom
Noah Green
Panagiotis G. Ipeirotis
Luis Gravano
Clement T. Yu
Prasoon Sharma
Weiyi Meng
Yan Qin
Sally E. Howe
David C. Nagel
Ching-chih Chen
Stephen M. Griffin
James Lightbourne
Walter L. Warnick
James W. Cooper
Mahesh Viswanathan
Donna K. Byron
Margaret Chan
John H. L. Hansen
J. R. Deller
Michael S. Seadle
J. R. Deller
Aparna Gurijala
Michael S. Seadle
Donald Byrd
Mingchun Liu
Chunru Wan
Matthew J. Dovey
Terry Sullivan
Anselmo Peñas
Julio Gonzalo
Felisa Verdejo
Ghaleb Abdulla
Chuck Baldwin
Terence Critchlow
Roy Kamimura
Ida Lozares
Ron Musick
Nu Ai Tang
Byung Suk Lee
Robert R. Snapp
Judith Klavans
Brian Whitman
Maayan Geffet
Dror G. Feitelson
James Ze Wang
Yanping Du
Brandon Muramatsu
Cathryn Manduca
Marcia A. Mardis
James H. Lightbourne
Flora P. McMartin
Marc Nanard
Jocelyne Nanard
Michael G. Christel
Bryan Maher
Andrew Begun
Jezekiel Ben-Arie
Purvin Pandit
Shyamsundar Rajaram
Michael L. Nelson
Gary Marchionini
Gary Geisler
Meng Yang
Alan F. Smeaton
Noel Murphy
Noel E. O'Connor
Seán Marlow
Hyowon Lee
Kieran McDonald
Paul Browne
Jiamin Ye
Andy Dong
Alice M. Agogino
ByungHoon Kang
Robert Wilensky
Kathleen McKeown
Shih-Fu Chang
James J. Cimino
Steven Feiner
Carol Friedman
Luis Gravano
Vasileios Hatzivassiloglou
Steven Johnson
Desmond A. Jordan
Judith Klavans
André Kushniruk
Vimla L. Patel
Simone Teufel
Elaine G. Toms
Joan C. Bartlett
Jeff Anderson-Lee
Robert Wilensky
David M. Levy
William Y. Arms
Oren Etzioni
Diane Nester
Barbara Tillett
Raymond A. Lorie
Brian F. Cooper
Hector Garcia-Molina
Arturo Crespo
Hector Garcia-Molina
Daniel Faensen
Lukas Faulstich
Heinz Schweppe
Annika Hinze
Alexander Steidinger
Tracy Riggs
Robert Wilensky
Richard J. Marisa
Gary Marchionini
Anne Craig
Larry Brandt
Judith Klavans
Hsinchun Chen
Allison Druin
Benjamin B. Bederson
Juan Pablo Hourcade
Lisa Sherman
Glenda Revelle
Michele Platner
Stacy Weng
Yin Leng Theng
Norliza Mohd-Nasir
George Buchanan
Bob Fields
Harold W. Thimbleby
Noel Cassidy
Tamara Sumner
Melissa Dawe
Gregory R. Crane
David A. Smith
Clifford E. Wulfman
Jeffrey A. Rydberg-Cox
Anne Mahoney
Gregory R. Crane
Michael S. Brown
W. Brent Seales
Richard Furuta
Shueh-Cheng Hu
Siddarth Kalasapur
Rajiv Kochumman
Eduardo Urbina
Ricardo Vivancos-Pérez
David Bainbridge
Gerry Bernbom
Mary Wallace
Andrew Dillon
Matthew J. Dovey
Jon W. Dunn
Michael Fingerhut
Ichiro Fujinaga
Eric J. Isaacson
Jen-Shin Hong
Bai-Hsuen Chen
Jieh Hsiang
Tien-Yu Hsu
Christopher R. Palmer
J. Pesenti
Raúl E. Valdés-Pérez
Michael G. Christel
Alexander G. Hauptmann
Tobun Dorbin Ng
Howard D. Wactlar
Jon W. Dunn
Eric J. Isaacson
Mark Derthick
Panagiotis G. Ipeirotis
Luis Gravano
Mehran Sahami
Noemie Elhadad
Min-Yen Kan
Simon Lok
Smaranda Muresan
Shahram Ebadollahi
Shih-Fu Chang
Edward N. Zalta
Colin Allen
Uri Nodelman
Matthew J. Dovey
Hussein Suleman
Christopher Klaus
Keith Andrew
Elizabeth D. Liddy
Stuart A. Sutton
Woojin Paik
Eileen Allen
Sarah Harwell
Michelle Monsour
Anne M. Turner
Jennifer Liddy
Judy C. Gilmore
Valerie S. Allen
Eiji Ikoma
Taikan Oki
Masaru Kitsuregawa
W. Harry Plantinga
Michael J. Bass
Margret Branschofsky
Kobus Barnard
David A. Forsyth
K. R. Debure
A. S. Russell
Cheng Jiun Yuan
W. Brent Seales
Wing Hang Cheung
Michael R. Lyu
Kam-Wing Ng
Kimberly S. Roempler
Carol Hansen Montgomery
Linda S. Marion
Taku A. Tokuyasu
Bruce R. Barkstrom
Alejandro Bia
Michael P. D'Alessandro
Richard S. Bakalar
Donna M. D'Alessandro
Denis E. Ashley
Mary J. C. Hendrix
Katy Börner
Chaomei Chen
Nina Wacholder
Craig G. Nevill-Manning
Paul Thompson
Traugott Koch
John Carter
Heike Neuroth
Ed O'Neill
Dagobert Soergel
Su-Shing Chen
Ching-chih Chen
Lucy T. Nowell
Elizabeth G. Hetzler
