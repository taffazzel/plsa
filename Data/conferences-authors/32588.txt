Association for Computational Linguistic, 39th Annual Meeting and 10th Conference of the European Chapter, Proceedings of the Conference, July 9-11, 2001, Toulouse, France.
Richard Durbin
Jean-Luc Gauvain
Lori Lamel
Gilles Adda
Martine Adda-Decker
Claude Barras
Langzhou Chen
Yannick de Kercadio
Fredy A. Amaya
José-Miguel Benedí
Timothy Baldwin
Michele Banko
Eric Brill
Catalina Barbu
Ruslan Mitkov
François Barthélemy
Pierre Boullier
Philippe Deschamp
Eric Villemonte de la Clergerie
Regina Barzilay
Kathleen McKeown
Gann Bierner
Rens Bod
Manuel Bodirsky
Katrin Erk
Alexander Koller
Joachim Niehren
Michael R. Brent
Xiaopeng Tao
Jill Burstein
Daniel Marcu
Slava Andreyev
Martin Chodorow
Lynne J. Cahill
John A. Carroll
Roger Evans
Daniel S. Paiva
Richard Power
Donia Scott
Kees van Deemter
Justine Cassell
Yukiko I. Nakano
Timothy W. Bickmore
Candace L. Sidner
Charles Rich
Eugene Charniak
David Chiang
Ann A. Copestake
Alex Lascarides
Dan Flickinger
Simon Corston-Oliver
Michael Gamon
Chris Brockett
Philippe de Groote
Alexander Ja. Dikovsky
John Dowding
Beth Ann Hockey
Jean Mark Gawron
Christopher Culy
Pablo Ariel Duboué
Kathleen McKeown
Denys Duchier
Ralph Debusmann
Stefan Evert
Brigitte Krenn
Atsushi Fujii
Tetsuya Ishikawa
Ismael García-Varea
Franz Josef Och
Hermann Ney
Francisco Casacuberta
Claire Gardent
Stefan Thater
Kim Gerdes
Sylvain Kahane
Ulrich Germann
Michael Jahr
Kevin Knight
Daniel Marcu
Kenji Yamada
Jonathan Ginzburg
Robin Cooper
Rebecca Green
Lisa Pearl
Bonnie J. Dorr
Philip Resnik
Claire Grover
Alex Lascarides
Jan Hajic
Pavel Krbec
Pavel Kveton
Karel Oliva
Vladimir Petkevic
Eva Hajicová
Petr Sgall
Sanda M. Harabagiu
Dan I. Moldovan
Marius Pasca
Rada Mihalcea
Mihai Surdeanu
Razvan C. Bunescu
Roxana Girju
Vasile Rus
Paul Morarescu
Daniel Hardt
Owen Rambow
Jing Huang
Geoffrey Zweig
Mukund Padmanabhan
Nancy Ide
Laurent Romary
Hideki Isozaki
Mark Johnson
Alexandra Kinyon
Dan Klein
Christopher D. Manning
Sandra Kübler
Erhard W. Hinrichs
Maria Lapata
Frank Keller
Scott McDonald
Alain Lecomte
Christian Retoré
Diane J. Litman
Julia Hirschberg
Marc Swerts
Jimin Liu
Tat-Seng Chua
Daniel Marcu
Lisa N. Michaud
Kathleen F. McCoy
Dan I. Moldovan
Vasile Rus
Karin Müller
Gerald Penn
Georgios Petasis
Frantz Vichot
Francis Wolinski
Georgios Paliouras
Vangelis Karkaletsis
Constantine D. Spyropoulos
Owen Rambow
Monica Rogati
Marilyn A. Walker
Ehud Reiter
Roma Robertson
A. Scott Lennox
Liesl Osman
Diana Santos
Paulo Rocha
Barry Schiffman
Inderjeet Mani
Kristian J. Concepcion
Helmut Schmid
Mats Rooth
William Schuler
Kyriakos N. Sgarbas
Nikos Fakotakis
George K. Kokkinakis
Matthew G. Snover
Michael R. Brent
Masao Utiyama
Hitoshi Isahara
Antal van den Bosch
Emiel Krahmer
Marc Swerts
Marcel P. van Lohuizen
Marilyn A. Walker
Rebecca J. Passonneau
Julie E. Boland
Kenji Yamada
Kevin Knight
Hirofumi Yamamoto
Shuntaro Isogai
Yoshinori Sagisaka
Tong Zhang
Fred Damerau
David Johnson
Ingrid Zuckerman
Eric Horvitz
