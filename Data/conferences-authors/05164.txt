Proceedings of the 53rd Annual Meeting of the Association for Computational Linguistics and the 7th International Joint Conference on Natural Language Processing of the Asian Federation of Natural Language Processing, ACL 2015, July 26-31, 2015, Beijing, China, Volume 2: Short Papers.
José Camacho-Collados
Mohammad Taher Pilehvar
Roberto Navigli
Julien Subercaze
Christophe Gravier
Frédérique Laforest
Tao Chen
Ruifeng Xu
Yulan He
Xuan Wang
Nghia The Pham
Angeliki Lazaridou
Marco Baroni
Shoushan Li
Lei Huang
Jingjing Wang
Guodong Zhou
Min Yang
Yinfei Yang
Yaowei Yan
Minghui Qiu
Forrest Sheng Bao
Matt Taddy
Zhongyu Wei
Yang Liu
Chen Li
Wei Gao
Ellie Pavlick
Juri Ganitkevitch
Tsz Ping Chan
Xuchen Yao
Benjamin Van Durme
Chris Callison-Burch
Goran Glavas
Sanja Stajner
Romina Altamirano
Thiago Castro Ferreira
Ivandré Paraboni
Luciana Benotti
Ivandré Paraboni
Michelle Reis Galindo
Douglas Iacovelli
Ina Roesiger
Arndt Riester
Robert Fisher
Reid G. Simmons
Ajda Gokcen
Marie-Catherine de Marneffe
Jacob Devlin
Hao Cheng
Hao Fang
Saurabh Gupta
Li Deng
Xiaodong He
Geoffrey Zweig
Margaret Mitchell
Semih Yagcioglu
Erkut Erdem
Aykut Erdem
Ruket Cakici
Grzegorz Chrupala
Ákos Kádár
Afra Alishahi
Douwe Kiela
Laura Rimell
Ivan Vulic
Stephen Clark
Yulia Tsvetkov
Chris Dyer
Heng Yu
Xuan Zhu
Sho Hoshino
Yusuke Miyao
Katsuhito Sudoh
Katsuhiko Hayashi
Masaaki Nagata
Takuya Matsuzaki
Akira Fujita
Naoya Todo
Noriko H. Arai
Boxing Chen
Hongyu Guo
Rajen Chatterjee
Marion Weller
Matteo Negri
Marco Turchi
Jiatao Gu
Victor O. K. Li
Jianfei Yu
Jing Jiang
Mingbo Ma
Liang Huang
Bowen Zhou
Bing Xiang
Zsolt Bitvai
Trevor Cohn
Jun Suzuki
Masaaki Nagata
Young-Bum Kim
Karl Stratos
Ruhi Sarikaya
Aliaksei Severyn
Massimo Nicosia
Gianni Barlacchi
Alessandro Moschitti
Robert Östling
Masoud Rouhizadeh
Emily Prud'hommeaux
Jan P. H. van Santen
Richard Sproat
Meghana Kshirsagar
Sam Thomson
Nathan Schneider
Jaime G. Carbonell
Noah A. Smith
Chris Dyer
Sheng Zhang
Yansong Feng
Songfang Huang
Kun Xu
Zhe Han
Dongyan Zhao
Douwe Kiela
Luana Bulat
Stephen Clark
Takaaki Tanaka
Masaaki Nagata
Rudolf Rosa
Zdenek Zabokrtský
Wenduan Xu
Michael Auli
Stephen Clark
Carlos Gómez-Rodríguez
Daniel Fernández-González
Fei Cheng
Kevin Duh
Yuji Matsumoto
Zeljko Agic
Dirk Hovy
Anders Søgaard
Roland Roller
Eneko Agirre
Aitor Soroa
Mark Stevenson
Ying Xu
Christoph Ringlstetter
Mi-Young Kim
Grzegorz Kondrak
Randy Goebel
Yusuke Miyao
Yang Liu
Furu Wei
Sujian Li
Heng Ji
Ming Zhou
Houfeng Wang
Dani Yogatama
Daniel Gillick
Nevena Lazic
Jennifer D'Souza
Vincent Ng
Gabriel Stanovsky
Ido Dagan
Mausam
Yaqin Yang
Yalin Liu
Nianwen Xue
Rob Voigt
Dan Jurafsky
Chen Chen
Vincent Ng
Weiren Yu
Julie A. McCann
Yiannos Stathopoulos
Simone Teufel
Zhenzhong Zhang
Le Sun
Xianpei Han
Linmei Hu
Xuzhong Wang
Mengdi Zhang
Juan-Zi Li
Xiaoli Li
Chao Shao
Jie Tang
Yongbin Liu
Peng Wang
Jiaming Xu
Bo Xu
Cheng-Lin Liu
Heng Zhang
Fangyuan Wang
Hongwei Hao
Egoitz Laparra
Itziar Aldabe
German Rigau
Thien Huu Nguyen
Ralph Grishman
Ofer Bronstein
Ido Dagan
Qi Li
Heng Ji
Anette Frank
Nanyun Peng
Mo Yu
Mark Dredze
Kenneth Heafield
Rohan Kshirsagar
Santiago Barona
Ayah Zirikly
Kai Liu
Hua Wang
Sajib Dasgupta
Ellie Pavlick
Travis Wolfe
Pushpendre Rastogi
Chris Callison-Burch
Mark Dredze
Benjamin Van Durme
Matthias Liebeck
Stefan Conrad
Ran Levy
Liat Ein-Dor
Shay Hummel
Ruty Rinott
Noam Slonim
Ellie Pavlick
Pushpendre Rastogi
Juri Ganitkevitch
Benjamin Van Durme
Chris Callison-Burch
Alina Maria Ciobanu
Liviu P. Dinu
Dallas Card
Amber E. Boydstun
Justin H. Gross
Philip Resnik
Noah A. Smith
Michel Galley
Chris Brockett
Alessandro Sordoni
Yangfeng Ji
Michael Auli
Chris Quirk
Margaret Mitchell
Jianfeng Gao
Bill Dolan
Minghua Nuo
Huidan Liu
Congjun Long
Jian Wu
Tong Wang
Abdelrahman Mohamed
Graeme Hirst
Manaal Faruqui
Chris Dyer
Olivier Ferret
Kristina Gulordava
Paola Merlo
Benoît Crabbé
Dirk Hovy
Anders Søgaard
Weizheng Chen
Jinpeng Wang
Yan Zhang
Hongfei Yan
Xiaoming Li
Shiliang Zhang
Hui Jiang
Mingbin Xu
Junfeng Hou
Li-Rong Dai
Khaled Aldebei
Xiangjian He
Jie Yang
Tong Wang
Vish Viswanath
Ping Chen
Piotr Mirowski
Andreas Vlachos
Michal Lukasik
Trevor Cohn
Kalina Bontcheva
Min Xiao
Yuhong Guo
Marco Turchi
Matteo Negri
Marcello Federico
Baotian Hu
Zhaopeng Tu
Zhengdong Lu
Hang Li
Qingcai Chen
Jingyi Zhang
Masao Utiyama
Eiichiro Sumita
Hai Zhao
Malte Nuhn
Julian Schamper
Hermann Ney
Benjamin Marie
Aurélien Max
Marlies van der Wees
Arianna Bisazza
Wouter Weerkamp
Christof Monz
Tianze Shi
Zhiyuan Liu
Yang Liu
Maosong Sun
Akiva Miura
Graham Neubig
Sakriani Sakti
Tomoki Toda
Satoshi Nakamura
Xiaojun Wan
Yue Hu
Anggi Maulidyani
Ruli Manurung
Courtney Napoles
Keisuke Sakaguchi
Matt Post
Joel R. Tetreault
Xinlei Shi
Junjie Zhai
Xudong Yang
Zehua Xie
Chao Liu
Chak Yan Yeung
John Lee
Aditya Joshi
Abhijit Mishra
Balamurali A. R
Pushpak Bhattacharyya
Mark James Carman
Yusheng Xie
Pranjal Daga
Yu Cheng
Kunpeng Zhang
Ankit Agrawal
Alok N. Choudhary
Yanran Li
Wenjie Li
Sujian Li
Rui Yan
Xiang Li
Mengwen Liu
Xiaohua Hu
Afshin Rahimi
Trevor Cohn
Timothy Baldwin
Luís Marujo
Wang Ling
Isabel Trancoso
Chris Dyer
Alan W. Black
Anatole Gershman
David Martins de Matos
João Paulo da Silva Neto
Jaime G. Carbonell
Jihen Karoui
Farah Benamara
Véronique Moriceau
Nathalie Aussenac-Gilles
Lamia Hadrich Belguith
Fan Zhang
Kui Xu
Luchen Tan
Haotian Zhang
Charles L. A. Clarke
Mark D. Smucker
Dakui Zhang
Yu Mao
Yang Liu
Hanshi Wang
Chuyuan Wei
Shiping Tang
Tao Ge
Heng Ji
Baobao Chang
Zhifang Sui
John Sylak-Glassman
Christo Kirov
David Yarowsky
Roger Que
Boris Galitsky
Dmitry I. Ilvovsky
Sergei O. Kuznetsov
Alberto Barrón-Cedeño
Simone Filice
Giovanni Da San Martino
Shafiq R. Joty
Lluís Màrquez
Preslav Nakov
Alessandro Moschitti
Cícero Nogueira dos Santos
Luciano Barbosa
Dasha Bogdanova
Bianca Zadrozny
Hai Wang
Mohit Bansal
Kevin Gimpel
David A. McAllester
Di Wang
Eric Nyberg
Xiaoqiang Zhou
Baotian Hu
Qingcai Chen
Buzhou Tang
Xiaolong Wang
Ivan Vulic
Marie-Francine Moens
Dana Rubinstein
Effi Levi
Roy Schwartz
Ari Rappoport
Daniel Fried
Tamara Polajnar
Stephen Clark
Wei Lu
Shohini Bhattasali
Jeremy Cytryn
Elana Feldman
Joonsuk Park
Zhiyuan Chen
Nianzu Ma
Bing Liu
Aditya Joshi
Vinita Sharma
Pushpak Bhattacharyya
Zhongqing Wang
Sophia Yat Mei Lee
Shoushan Li
Guodong Zhou
Mohammad Al Boni
Keira Zhou
Hongning Wang
Matthew S. Gerber
Yung-Chun Chang
Cen-Chieh Chen
Yu-Lun Hsieh
Chien Chin Chen
Wen-Lian Hsu
Patrik Lambert
Liang-Chih Yu
Jin Wang
K. Robert Lai
Xue-jie Zhang
Nikola Mrksic
Diarmuid Ó Séaghdha
Blaise Thomson
Milica Gasic
Pei-hao Su
David Vandyke
Tsung-Hsien Wen
Steve J. Young
Wendong Ge
Bo Xu
Young-Bum Kim
Karl Stratos
Xiaohu Liu
Ruhi Sarikaya
Nikolina Koleva
Martin Villalba
Maria Staudte
Alexander Koller
Changsu Lee
Youngjoong Ko
Jungyun Seo
Sanja Stajner
Hannah Béchara
Horacio Saggion
Ziqiang Cao
Furu Wei
Sujian Li
Wenjie Li
Ming Zhou
Houfeng Wang
Sandro Bauer
Simone Teufel
Natalie Schluter
Anders Søgaard
Long Duong
Trevor Cohn
Steven Bird
Paul Cook
Yuichiro Sawai
Hiroyuki Shindo
Yuji Matsumoto
Chuan Wang
Nianwen Xue
Sameer Pradhan
Jan Buys
Phil Blunsom
Yonatan Bisk
Christos Christodoulopoulos
Julia Hockenmaier
Alireza Nourian
Mohammad Sadegh Rasooli
Mohsen Imany
Heshaam Faili
