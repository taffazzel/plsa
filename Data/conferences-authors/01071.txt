The 50th Annual Meeting of the Association for Computational Linguistics, Proceedings of the Conference, July 8-14, 2012, Jeju Island, Korea - Volume 1: Long Papers.
Kevin Duh
Katsuhito Sudoh
Xianchao Wu
Hajime Tsukada
Masaaki Nagata
Patrick Simianer
Stefan Riezler
Chris Dyer
Prasanth Kolachina
Nicola Cancedda
Marc Dymetman
Sriram Venkatapathy
Khe Chai Sim
Chia-ying Lee
James R. Glass
Xingyuan Peng
Dengfeng Ke
Bo Xu
Vanessa Wei Feng
Graeme Hirst
Yuping Zhou
Nianwen Xue
Viet-An Nguyen
Jordan L. Boyd-Graber
Philip Resnik
Oleksandr Kolomiyets
Steven Bethard
Marie-Francine Moens
Nathanael Chambers
Guillermo Garrido
Anselmo Peñas
Bernardo Cabaleiro
Álvaro Rodrigo
Jonathan Berant
Ido Dagan
Meni Adler
Jacob Goldberger
S. R. K. Branavan
Nate Kushman
Tao Lei
Regina Barzilay
Elia Bruni
Gemma Boleda
Marco Baroni
Nam-Khanh Tran
Spence Green
John DeNero
Malte Nuhn
Arne Mauser
Hermann Ney
Graham Neubig
Taro Watanabe
Shinsuke Mori
Tatsuya Kawahara
Ariya Rastrow
Mark Dredze
Sanjeev Khudanpur
Micha Elsner
Sharon Goldwater
Jacob Eisenstein
Hao Tang
Joseph Keshet
Karen Livescu
Matthieu Constant
Anthony Sigogne
Patrick Watrin
Wenliang Chen
Min Zhang
Haizhou Li
Shay B. Cohen
Karl Stratos
Michael Collins
Dean P. Foster
Lyle H. Ungar
Weiwei Sun
Xiaojun Wan
Weiwei Sun
Hans Uszkoreit
Xu Sun
Houfeng Wang
Wenjie Li
Danilo Croce
Alessandro Moschitti
Roberto Basili
Martha Palmer
Zhi Zhong
Hwee Tou Ng
Asher Stern
Roni Stern
Ido Dagan
Ariel Felner
Xiaodong He
Li Deng
Shujie Liu
Chi-Ho Li
Mu Li
Ming Zhou
Ashish Vaswani
Liang Huang
David Chiang
Arjun Mukherjee
Bing Liu
Asli Çelikyilmaz
Dilek Hakkani-Tür
Arjun Mukherjee
Bing Liu
Sindhu Raghavan
Raymond J. Mooney
Hyeonseo Ku
Polina Kuznetsova
Vicente Ordonez
Alexander C. Berg
Tamara L. Berg
Yejin Choi
Ioannis Konstas
Mirella Lapata
Michael L. Wick
Sameer Singh
Andrew McCallum
Mohit Bansal
Dan Klein
Amjad Abu-Jbara
Pradeep Dasigi
Mona T. Diab
Dragomir R. Radev
Fangtao Li
Sinno Jialin Pan
Ou Jin
Qiang Yang
Xiaoyan Zhu
Thomas Lippincott
Anna Korhonen
Diarmuid Ó Séaghdha
David Chen
Hiroyuki Shindo
Yusuke Miyao
Akinori Fujino
Masaaki Nagata
Fan Bu
Hang Li
Xiaoyan Zhu
Jinsong Su
Hua Wu
Haifeng Wang
Yidong Chen
Xiaodong Shi
Huailin Dong
Qun Liu
Hassan Sajjad
Alexander M. Fraser
Helmut Schmid
Arianna Bisazza
Marcello Federico
Bevan K. Jones
Mark Johnson
Sharon Goldwater
Dominick Ng
James R. Curran
Andreas Maletti
Joost Engelfriet
Rui Yan
Mirella Lapata
Xiaoming Li
Xiaohua Liu
Ming Zhou
Xiangyang Zhou
Zhongyang Fu
Furu Wei
Qiming Diao
Jing Jiang
Feida Zhu
Ee-Peng Lim
Gregory Druck
Bo Pang
Zhonghua Qu
Yang Liu
Patrick Pantel
Thomas Lin
Michael Gamon
Xinfan Meng
Furu Wei
Xiaohua Liu
Ming Zhou
Ge Xu
Houfeng Wang
Wen Chan
Xiangdong Zhou
Wei Wang
Tat-Seng Chua
Claire Gardent
Shashi Narayan
Geoffrey Zweig
John C. Platt
Christopher Meek
Christopher J. C. Burges
Ainur Yessenalina
Qiang Liu
Zhiheng Huang
Yi Chang
Bo Long
Jean-François Crespo
Anlei Dong
Sathiya Keerthi
Su-Lin Wu
Max Whitney
Anoop Sarkar
Tahira Naseem
Regina Barzilay
Amir Globerson
Shomir Wilson
Ivan Titov
Alexandre Klementiev
Katsuhiko Hayashi
Taro Watanabe
Masayuki Asahara
Yuji Matsumoto
Makoto Kanazawa
Sylvain Salvati
Zhenghua Li
Ting Liu
Wanxiang Che
Dani Yogatama
Yanchuan Sim
Noah A. Smith
Sungchul Kim
Kristina Toutanova
Hwanjo Yu
Gözde Özbal
Carlo Strapparava
Limin Yao
Sebastian Riedel
Andrew McCallum
Shingo Takamatsu
Issei Sato
Hiroshi Nakagawa
Rémy Kessler
Xavier Tannier
Caroline Hagège
Véronique Moriceau
André Bittar
William Yang Wang
Elijah Mayfield
Suresh Naidu
Jeremiah Dittmar
Xinyan Xiao
Deyi Xiong
Min Zhang
Qun Liu
Shouxun Lin
Alessandro Moschitti
Qi Ju
Richard Johansson
Emily Pitler
Seyed Abolghasem Mirroshandel
Alexis Nasr
Joseph Le Roux
Yaqin Yang
Nianwen Xue
Katja Markert
Yufang Hou
Michael Strube
Karin Mauge
Khash Rohanimanesh
Jean-David Ruvini
Alexandre Davis
Adriano Veloso
Altigran Soares da Silva
Alberto H. F. Laender
Wagner Meira Jr.
Ce Zhang
Feng Niu
Christopher Ré
Jude W. Shavlik
Wei Lu
Dan Roth
Einat Minkov
Luke S. Zettlemoyer
Ingrid Falk
Claire Gardent
Jean-Charles Lamirel
Weiwei Guo
Mona T. Diab
Eric H. Huang
Richard Socher
Christopher D. Manning
Andrew Y. Ng
Mark Johnson
Katherine Demuth
Michael C. Frank
Cristian Danescu-Niculescu-Mizil
Justin Cheng
Jon M. Kleinberg
Lillian Lee
Deyi Xiong
Min Zhang
Haizhou Li
Nan Yang
Mu Li
Dongdong Zhang
Nenghai Yu
Chang Liu
Hwee Tou Ng
Boxing Chen
Roland Kuhn
Samuel Larkin
Majid Razmara
George Foster
Baskaran Sankaran
Anoop Sarkar
Yang Feng
Dongdong Zhang
Mu Li
Qun Liu
Adam Pauls
Dan Klein
Hiroshi Yamaguchi
Kumiko Tanaka-Ishii
Wei He
Hua Wu
Haifeng Wang
Ting Liu
Marco Guerini
Carlo Strapparava
Oliviero Stock
Eduard C. Dragut
Hong Wang
Clement T. Yu
A. Prasad Sistla
Weiyi Meng
Ziheng Lin
Chang Liu
Hwee Tou Ng
Min-Yen Kan
Sander Wubben
Antal van den Bosch
Emiel Krahmer
Hyun-Je Song
Jeong Woo Son
Tae-Gil Noh
Seong-Bae Park
Sang-Jo Lee
Fei Liu
Fuliang Weng
Xiao Jiang
Jun Hatori
Takuya Matsuzaki
Yusuke Miyao
Jun'ichi Tsujii
Qiuye Zhao
Mitch Marcus
