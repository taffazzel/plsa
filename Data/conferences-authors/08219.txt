1st International Symposium on 3D Data Processing Visualization and Transmission (3DPVT 2002), 19-21 June 2002, Padova, Italy.
Jan Neumann
Cornelia Fermüller
Yiannis Aloimonos
Silvio Savarese
Holly E. Rushmeier
Fausto Bernardini
Pietro Perona
Li Zhang
Brian Curless
Steven M. Seitz
Moez Bellamine
Norihiro Abe
Kazuaki Tanaka
Hirokazu Taki
Jan-Michael Frahm
Jan-Friso Evers-Senne
Reinhard Koch
Delfina Malandrino
Gennaro Meo
Giuseppina Palmieri
Vittorio Scarano
Emmanuel Jay
Paul F. Lister
Martin White
Andrea Giachetti
Massimiliano Tuveri
Gianluigi Zanetti
Jean-Marie Bouteiller
Michel Baudry
Mikael Jern
S. Palmberg
M. Ranlöf
Wen Qi
Zhijian Song
Guoqiang Wu
Johan Montagnat
Eduardo Davila
Isabelle E. Magnin
David Lloret
Joan Serrat
Antonio M. López
Juan José Villanueva
A. Doi
S. Fujiwara
K. Matsuda
M. Kameda
Jianlong Zhou
Manfred Hinz
Klaus D. Tönnies
Michal Kozubek
Petr Matula
Heinz Eipel
Michael Hausmann
Michal Kozubek
Petr Matula
Pavel Matula
Petr Mejzlík
Bing-Bing Chai
Sriram Sethuraman
Harpreet S. Sawhney
Eugene Borovikov
Larry S. Davis
Linmi Tao
Vittorio Murino
Gérard G. Medioni
Avi Kak
Christina Pavlopoulou
Dragan Tubic
Patrick Hébert
Denis Laurendeau
Carlos Hernández Esteban
Francis Schmitt
Ebroul Izquierdo
Louis-Philippe Morency
Ali Rahimi
Trevor Darrell
Wei Zhang
Jana Kosecká
Fayin Li
Annie Yao
Andrew Calway
L. Irene Cheng
Anup Basu
Sotiris Malassiotis
Niki Aifanti
Michael G. Strintzis
Akihiro Miyakawa
Masamitsu Sugimoto
Mikako Hosokawa
Yoshitaka Shibata
Andreas Baudry
Michael Bungenstock
Bärbel Mertsching
Alison Luo
Ahmed Amer
Scott Speidel
Darrell D. E. Long
Alex Pang
Nobuyuki Kita
Yasuyo Kita
Hai-quan Yang
Davide Rocchesso
Flavia Sparacino
Christopher Richard Wren
Ali Azarbayejani
Alex Pentland
Takashi Matsuyama
Takeshi Takai
Adrian Hilton
Jonathan Starck
Gordon Collins
Eyal Hameiri
Ilan Shimshoni
Eric Paquet
Herna L. Viktor
Shawn Peter
Carlo Colombo
Alberto Del Bimbo
Federico Pernici
Luc J. Van Gool
D. Vandemeulen
Gregor A. Kalberer
Tinne Tuytelaars
Alexey Zalesny
Eric Badiqué
Giovanni Bazzoni
Enrico Bianchi
Oliver Grau
Alec Knox
Reinhard Koch
Fabio Lavagetto
Alex Parkinson
Federico Pedersini
Augusto Sarti
Graham A. Thomas
Stefano Tubaro
André Redert
Marc Op de Beeck
Christoph Fehn
Wijnand A. IJsselsteijn
Marc Pollefeys
Luc J. Van Gool
Eyal Ofek
Ian Sexton
Philip Surman
Graham A. Thomas
Oliver Grau
Bang Jun Lei
Emile A. Hendriks
Vassilios Vlahakis
John Karigiannis
Nikolaos Ioannidis
Manolis Tsotros
Michael Gounaris
Didier Stricker
Patrick Dähne
Luís Almeida
Neil H. McCormick
Robert B. Fisher
Juan Ruiz de Miras
Francisco R. Feito
Silvio Savarese
Min Chen
Pietro Perona
Augusto Sarti
Stefano Tubaro
Milos Srámek
Leonid I. Dimitrov
Matheen Siddiqui
Stan Sclaroff
Paula N. Mallón
Montserrat Bóo
Margarita Amor
Javier D. Bruguera
Dietmar Saupe
Jens-Peer Kuska
Frédéric Payan
Marc Antonini
Osamu Ikeda
Réjean Baribeau
Jan J. Koenderink
Sylvia C. Pont
Christoph Strecha
Luc J. Van Gool
James Davis
Stephen R. Marschner
Matt Garr
Marc Levoy
Takeshi Masuda
Thomas Bülow
Hanan Samet
Andrzej Kochut
Daniel E. Laney
Martin Bertram
Mark A. Duchaineau
Nelson L. Max
Stephan Bischoff
Leif Kobbelt
Jean-François Lapointe
Emanuele Danovaro
Leila De Floriani
Akihiro Miyakawa
Masamitsu Sugimoto
Mikako Hosokawa
Yoshitaka Shibata
Piotr Garbat
Malgorzata Kujawinska
Xiangchuan Wang
Gerald Jaeschke
Matthias Hemmje
Riccardo Bernardini
Guido M. Cortelazzo
Giampaolo Tormen
André Redert
Emile A. Hendriks
Jan Biemond
Sabry F. El-Hakim
J.-Angelo Beraldin
Jean-François Lapointe
Antonio Adán
Santiago Salamanca
Carlos Cerrada
Pilar Merchán
Tetsu Kajita
Hiroyuki Kaneko
Koichi Fukuda
Akira Kawanaka
Timothée Jost
Heinz Hügli
Fred W. DePiero
Salvatore Curti
Daniele Sirtori
Filippo Vella
Ulas Yilmaz
Adem Yasar Mülayim
Volkan Atalay
Craig Robertson
Robert B. Fisher
Marco Callieri
Paolo Cignoni
Claudio Rocchini
Roberto Scopigno
John Isidoro
Stan Sclaroff
Andrew E. Johnson
Roberto Manduchi
Yiyong Sun
David L. Page
Joon Ki Paik
Andreas F. Koschan
Mongi A. Abidi
Gabriel Taubin
Boris Kronrod
Craig Gotsman
Martin Isenburg
Jack Snoeyink
Anthony J. Yezzi
Gregory G. Slabaugh
Adrian Broadhurst
Roberto Cipolla
Ronald W. Schafer
Sung Ha Kang
Tony F. Chan
Stefano Soatto
Hailin Jin
Anthony J. Yezzi
Stefano Soatto
Atsushi Imiya
Kazuhiko Kawamoto
Naoufel Werghi
Yijun Xiao
Jan Sijbers
Dirk Van Dyck
Jean-Philippe Vandeborre
Vincent Couillet
Mohamed Daoudi
Guy Froimovich
Ehud Rivlin
Ilan Shimshoni
Eyal Hameiri
Ilan Shimshoni
Karl J. Sharman
Mark S. Nixon
John N. Carter
Stefano Arrighetti
Stefano Guanziroli
Ennio Ottaviani
Umberto Castellani
Salvatore Livatino
Robert B. Fisher
Pawel Drapikowski
Tomasz Nowakowski
N. Alberto Borghese
Paolo Rigiroli
Nikos Grammalidis
L. Bleris
Michael G. Strintzis
Raffaella Lanzarotti
N. Alberto Borghese
Paola Campadelli
Gozde B. Unal
Hamid Krim
Anthony J. Yezzi
Cecilia Mazzaro
Mario Sznaier
Octavia I. Camps
Stefano Soatto
Alessandro Bissacco
Gregory G. Slabaugh
Ronald W. Schafer
Mat C. Hans
Steven W. Zucker
Gang Li
Pengyu Hong
Zhen Wen
Thomas S. Huang
Heung-Yeung Shum
Xiaolin Feng
Pietro Perona
Michele Vendruscolo
Baohong Zhang
Adam Godzik
Alberto Caprara
Giuseppe Lancia
Gianluigi Ciocca
Raimondo Schettini
Damian Green
John Cosmas
Take Itagaki
Martin Kampel
Srdan Tosovic
Robert Sablatnig
J.-Angelo Beraldin
Gabriele Guidi
S. Ciofi
Carlo Atzeni
Francesca Pozzi
N. Tsirliganis
George Pavlidis
Anestis Koutsoudis
D. Papadopoulou
A. Tsompanopoulos
Kostas Stavroglou
Z. Loukou
Christodoulos Chamzas
Conny Riani Gunadi
Hiroyuki Shimizu
Kazuya Kodama
Kiyoharu Aizawa
Vítor Sequeira
João G. M. Gonçalves
Guy Caspary
Yehoshua Y. Zeevi
João Filipe Ferreira
Jorge Lobo
Jorge Dias
Pavel Matula
Robert J. T. Sadleir
Paul F. Whelan
Cédric Marchessoux
Noël Richard
Christine Fernandez-Maloigne
Riccardo Lattanzi
Marco Viceconti
Marco Petrone
Paolo Quadrani
Cinzia Zannoni
Concettina Guerra
Stefano Lonardi
Giuseppe Zanotti
Petko Faber
Bob Fisher
Christian Früh
Avideh Zakhor
Andreas Ullrich
Nikolaus Studnicka
Johannes Riegl
Simone Orlandini
