ACL 2009, Proceedings of the 47th Annual Meeting of the Association for Computational Linguistics and the 4th International Joint Conference on Natural Language Processing of the AFNLP, 2-7 August 2009, Singapore, Short Papers.
Shay B. Cohen
Noah A. Smith
Hideharu Okuma
Kazuo Hara
Masashi Shimbo
Yuji Matsumoto
Laura Kallmeyer
Wolfgang Maier
Yannick Parmentier
Emily Pitler
Ani Nenkova
Sangkeun Jung
Cheongjae Lee
Kyungduk Kim
Gary Geunbae Lee
Oi Yee Kwong
Chao-Lin Liu
Kan-Wen Tien
Min-Hua Lai
Yi-Hsuan Chuang
Shih-Hung Wu
Han-Cheol Cho
Do-Gil Lee
Jung-Tae Lee
Pontus Stenetorp
Jun'ichi Tsujii
Hae-Chang Rim
Navanath Saharia
Dhrubajyoti Das
Utpal Sharma
Jugal K. Kalita
Lilja Øvrelid
Jonas Kuhn
Kathrin Spreyer
Yoshihide Kato
Shigeki Matsubara
Matt Post
Daniel Gildea
Manabu Sassano
Sadao Kurohashi
Stephen Clark
James R. Curran
Dan Roth
Mark Sammons
V. G. Vinod Vydiswaran
Makoto Imamura
Yasuhiro Takayama
Nobuhiro Kaji
Masashi Toyoda
Masaru Kitsuregawa
Ioannis Korkontzelos
Suresh Manandhar
Lili Kotlerman
Ido Dagan
Idan Szpektor
Maayan Zhitomirsky-Geffet
Beñat Zapirain
Eneko Agirre
Lluís Màrquez
Milan Tofiloski
Julian Brooke
Maite Taboada
Sungjin Lee
Gary Geunbae Lee
Kenji Imamura
Kuniko Saito
Tomoko Izumi
Kazunori Komatani
Alexander I. Rudnicky
Shuguang Li
Suresh Manandhar
Aoife Cahill
Sourish Chaudhuri
Naman K. Gupta
Noah A. Smith
Carolyn Penstein Rosé
Rahul Katragadda
Vasudeva Varma
Michael Goodman
Francis Bond
Ouyang You
Wenjie Li
Qin Lu
Furu Wei
Wenjie Li
Yanxiang He
Zhongjun He
Yao Meng
Yajuan Lü
Hao Yu
Qun Liu
Hongfei Jiang
Muyun Yang
Tiejun Zhao
Sheng Li
Bo Wang
Yuejie Zhang
Yang Wang
Xiangyang Xue
Reinhard Rapp
Hao Xiong
Wenwen Xu
Haitao Mi
Yang Liu
Qun Liu
John DeNero
Adam Pauls
Dan Klein
Zdenek Zabokrtský
Martin Popel
Dipankar Das
Sivaji Bandyopadhyay
Seon Yang
Youngjoong Ko
Alexandra Balahur
Ester Boldrini
Andrés Montoyo
Patricio Martínez-Barco
Clinton Burfoot
Timothy Baldwin
Xipeng Qiu
Wenjun Gao
Xuanjing Huang
Hye-Jin Min
Jong C. Park
Viola Ganter
Michael Strube
Xinfan Meng
Houfeng Wang
Chau Q. Nguyen
Tuoi T. Phan
Chao Zhang
Nan Sun
Xia Hu
Tingzhu Huang
Tat-Seng Chua
Mamoru Komachi
Shimpei Makimoto
Kei Uchiumi
Manabu Sassano
Janara Christensen
Mausam
Oren Etzioni
Xiaoyin Wang
David Lo
Jing Jiang
Lu Zhang
Hong Mei
Liang-Chih Yu
Chien-Lung Chan
Chung-Hsien Wu
Chao-Cheng Lin
Hidetsugu Nanba
Haruka Taguma
Takahiro Ozaki
Daisuke Kobayashi
Aya Ishino
Toshiyuki Takezawa
Barbora Hladká
Jirí Mírovský
Pavel Schlesinger
Yuhang Yang
Tiejun Zhao
Qin Lu
Dequan Zheng
Hao Yu
István Varga
Shoichi Yokoyama
Nizar Habash
Ryan Roth
Christoph Tillmann
Yonggang Deng
Bowen Zhou
Gumwon Hong
Seung-Wook Lee
Hae-Chang Rim
Mei Yang
Jing Zheng
Yizhao Ni
Craig Saunders
Sándor Szedmák
Mahesan Niranjan
Chris Biemann
Monojit Choudhury
Animesh Mukherjee
Jonathan Chevelu
Thomas Lavergne
Yves Lepage
Thierry Moudenc
Weiwei Sun
Zhifang Sui
Meng Wang
Paramveer S. Dhillon
Lyle H. Ungar
Fei Liu
Yang Liu
Wai Kit Lo
Wenying Xiong
Helen M. Meng
Gina-Anne Levow
Hasim Sak
Tunga Güngör
Murat Saraclar
Timothy A. Miller
Luan Nguyen
William Schuler
Minwoo Jeong
Chin-Yew Lin
Gary Geunbae Lee
Fang-Lan Huang
Cho-Jui Hsieh
Kai-Wei Chang
Chih-Jen Lin
Yashar Mehdad
Hal Daumé III
Dingding Wang
Shenghuo Zhu
Tao Li
Yihong Gong
Alexander Koller
Kristina Striegnitz
Donna Byron
Justine Cassell
Robert Dale
Sara Dalzel-Job
Johanna D. Moore
Jon Oberlander
Eva Banik
Rada Mihalcea
Carlo Strapparava
Mahesh Joshi
Carolyn Penstein Rosé
Qiong Wu
Songbo Tan
Xueqi Cheng
Dae-Neung Sohn
Jung-Tae Lee
Hae-Chang Rim
Alexander Mikhailian
Tiphaine Dalmas
Rani Pinchuk
Yllias Chali
Sadid A. Hasan
Shafiq R. Joty
Wei-Yun Ma
Kathy McKeown
Phil Blunsom
Trevor Cohn
Sharon Goldwater
Mark Johnson
Taro Watanabe
Hajime Tsukada
Hideki Isozaki
Deniz Yuret
Ergun Biçici
Robert C. Moore
Chris Quirk
Cristina Mota
Ralph Grishman
Asad B. Sayeed
Tamer Elsayed
Nikesh Garera
David Alexander
Tan Xu
Douglas W. Oard
David Yarowsky
Christine D. Piatko
Javier Artiles
Julio Gonzalo
Enrique Amigó
Frank Reichartz
Hannes Korte
Gerhard Paass
Prashant Gupta
Heng Ji
