2nd International Symposium on 3D Data Processing, Visualization and Transmission (3DPVT 2004), 6-9 September 2004, Thessaloniki, Greece.
Sylvia C. Pont
Jan J. Koenderink
Jean-Yves Guillemaut
Ondrej Drbohlav
Radim Sára
John Illingworth
Hossein Ragheb
Edwin R. Hancock
Jan Erik Solem
Henrik Aanæs
Anders Heyden
J. Patrick Cavanagh
Michael von Grünau
Lee Zimmerman
Donald MacVicar
Stuart Ford
Ewan Borland
Robert Rixon
John W. Patterson
W. Paul Cockshott
Filareti Tsalakanidou
Sotiris Malassiotis
Michael G. Strintzis
Ioannis A. Ypsilos
Adrian Hilton
Aseel Turkmani
Philip J. B. Jackson
Craig Gotsman
Alla Sheffer
Vladislav Kraevoy
Marc Alexa
Ariel Shamir
D. Loganathan
K. M. Mehata
Jiantao Pu
Yi Liu
Guyu Xin
Hongbin Zha
Weibin Liu
Yusuke Uehara
Petros Daras
Dimitrios Zarpalas
Dimitrios Tzovaras
Michael G. Strintzis
Tatjana Zrimec
Sata Busayarat
Karsten Müller
Aljoscha Smolic
Philipp Merkle
Bernhard Kaspar
Peter Eisert
Thomas Wiegand
Sebastiano Battiato
Alessandro Capra
Salvatore Curti
Marco La Cascia
Özgür Ulucay
Sarp Ertürk
Tarik Filali Ansary
Jean-Philippe Vandeborre
Saïd Mahmoudi
Mohamed Daoudi
Konstantinos Moustakas
Dimitrios Tzovaras
Michael G. Strintzis
Maria F. Trujillo
Ebroul Izquierdo
Wen Qi
Heather J. Rea
Doug E. R. Clark
Jonathan R. Corney
Nick K. Taylor
Dragan Tubic
Patrick Hébert
Jean-Daniel Deschênes
Denis Laurendeau
Mario Castelán
Edwin R. Hancock
Gunnar Boström
Marco Fiocco
David Puig
A. Rossini
João G. M. Gonçalves
Vítor Sequeira
Anselmo Antunes Montenegro
Paulo Cezar Pinto Carvalho
Marcelo Gattass
Luiz Velho
Fang Meng
Hongbin Zha
Benjamin Bustos
Daniel A. Keim
Dietmar Saupe
Tobias Schreck
Dejan V. Vranic
Huynh Quang Huy Viet
Makoto Sato
Hiromi T. Tanaka
Angel Domingo Sappa
Nicola Moretto
Ruggero Frezza
Adrian G. Bors
Costas Panagiotakis
Georgios Tziritas
M. A. Zerafat Pisheh
A. Sheikhi
Antonio Adán
Fernando Molina
Luis Morena
Markus Feißt
Andreas Christ
Ali Lakhia
Vladimir Brajovic
Tim Bodenmüller
Gerd Hirzinger
Volker Blanz
Albert Mehl
Thomas Vetter
Hans-Peter Seidel
Jan Erik Solem
Fredrik Kahl
Ioannis P. Ivrissimtzis
Yunjin Lee
Seungyong Lee
Won-Ki Jeong
Hans-Peter Seidel
Hossein Madjidi
Shahriar Negahdaripour
Nuno Gonçalves
Helder Araújo
Dinkar Gupta
Ankita Kumar
Kostas Daniilidis
Ha Vu Le
Sudipta N. Sinha
Marc Pollefeys
Adrian Hilton
Jonathan Starck
Marcus A. Magnor
Bastian Goldlücke
German K. M. Cheung
Simon Baker
Jessica K. Hodgins
Takeo Kanade
Nadia Magnenat-Thalmann
Hyewon Seo
Alexander Neubeck
Alexey Zalesny
Luc J. Van Gool
Christian Früh
Russell Sammon
Avideh Zakhor
Nikos Komodakis
George Pagonis
George Tziritas
Thomas P. Koninckx
Indra Geys
Tobias Jaeggli
Luc J. Van Gool
François Blais
Michel Picard
Guy Godin
Voicu Popescu
Elisha Sacks
Gleb Bahmutov
David Nistér
Ioannis Stamos
Marius Leordeanu
Adnan Ansar
Andres Castano
Larry H. Matthies
Zoltán Megyesi
Dmitry Chetverikov
Stefano Ferrari
Iuri Frosio
Vincenzo Piuri
N. Alberto Borghese
Hiroshi Kawasaki
Ryo Furukawa
Szymon Rusinkiewicz
Antonio Robles-Kelly
Edwin R. Hancock
Jamie Cook
Vinod Chandran
Sridha Sridharan
Clinton Fookes
William A. P. Smith
Antonio Robles-Kelly
Edwin R. Hancock
Xiaotian Yan
Fang Meng
Hongbin Zha
Georg Biegelbauer
Markus Vincze
Indra Geys
Thomas P. Koninckx
Luc J. Van Gool
Pekka Forsman
Aarne Halme
Marco Andreetto
Guido Maria Cortelazzo
Luca Lucchese
Nobuyuki Bannai
Alexander Agathos
Robert B. Fisher
Shohei Nobuhara
Takashi Matsuyama
Paola Dalmasso
Roberto Nerino
Pavel Matula
Petr Matula
Michal Kozubek
Petr Mejzlík
Zoltán Szlávik
Laszlo Havasi
Tamás Szirányi
Nicola Brusco
Simone Carmignato
Marco Andreetto
Guido Maria Cortelazzo
Subhajit Sanyal
Mayank Bansal
Subhashis Banerjee
Prem Kumar Kalra
Akihiko Torii
Atsushi Imiya
Zsolt Jankó
Dmitry Chetverikov
Piotr Garbat
Marek Wegiel
Malgorzata Kujawinska
Jasmine Burguet
Najib Gadi
Isabelle Bloch
Jake K. Aggarwal
Sangho Park
Toshio Ueshiba
Fumiaki Tomita
Don Ray Murray
James J. Little
Ajith Mascarenhas
Martin Isenburg
Valerio Pascucci
Jack Snoeyink
Alexander Bogomjakov
Craig Gotsman
Daniel E. Laney
Valerio Pascucci
L. Irene Cheng
Pierre Boulanger
Xiangyang Ju
Zhili Mao
J. Paul Siebert
Nigel J. B. McFarlane
Jiahua Wu
Robin D. Tillett
Steven W. Zucker
A. Dobbins
Frédo Durand
Johnny Park
Avinash C. Kak
Andrew W. Fitzgibbon
Alex Rav-Acha
Shmuel Peleg
Philippos Mordohai
Gérard G. Medioni
Xenophon Zabulis
Kostas Daniilidis
Markus H. Gross
George Kamberov
Gerda Kamberova
Peter K. Allen
Steven Feiner
Alejandro J. Troccoli
Hrvoje Benko
Edward W. Ishak
Benjamin Smith
Benedict J. Brown
Szymon Rusinkiewicz
Marcelo Bertalmío
Pere Fort
Daniel Sánchez-Crespo
Margrit Gelautz
Danijela Markovic
Truong Kieu Linh
Atsushi Imiya
Ikuko Shimizu Okatani
Akihiro Sugimoto
L. Irene Cheng
Anup Basu
Marco Callieri
Andrea Fasano
Gaetano Impoco
Paolo Cignoni
Roberto Scopigno
G. Parrini
Giuseppe Biagini
Linmi Tao
Umberto Castellani
Vittorio Murino
Sebastian Knorr
Carsten Clemens
Matthias Kunter
Thomas Sikora
Levente Hajder
Dmitry Chetverikov
István Vajk
Raphaèle Balter
Patrick Gioia
Luce Morin
Franck Galpin
Robert E. Loke
J. M. Hans du Buf
J. N. Ellinas
Manolis S. Sangriotis
Antonio Robles-Kelly
Edwin R. Hancock
Angel Domingo Sappa
Georgii Khachaturov
Rafael Moncayo-Muños
Hossein Ragheb
Antonio Robles-Kelly
Edwin R. Hancock
Pezhman Firoozfam
Shahriar Negahdaripour
Yi Liu
Jiantao Pu
Hongbin Zha
Weibin Liu
Yusuke Uehara
Mikio Shinya
Angel Domingo Sappa
Niki Aifanti
Sotiris Malassiotis
Michael G. Strintzis
Muhammad Hussain
Yoshihiro Okada
Koichi Niijima
Leonid I. Dimitrov
Milos Srámek
D. Biccari
Diego Calabrese
D. Gurnett
R. Huff
L. Marinangeli
R. Jordan
E. Nielsen
G. G. Ori
Giovanni Picardi
Jeffrey J. Plaut
F. Provvedi
Roberto Seu
Enrico M. Zampolini
André Reder
George Barbastathis
Abhijit S. Ogale
Yiannis Aloimonos
Petros Daras
Dimitrios Zarpalas
Dimitrios Tzovaras
Michael G. Strintzis
Clinton Fookes
Anthony J. Maeder
Sridha Sridharan
Jamie Cook
Maria Petrou
Farahnaz Mohanna
Vassili A. Kovalev
Stefan Burkhardt
Kimmo Fredriksson
Tuomas Ojamies
Janne Ravantti
Esko Ukkonen
Ming-Ching Chang
Frederic F. Leymarie
Benjamin B. Kimia
Gabriel Peyré
Laurent D. Cohen
Takeshi Masuda
