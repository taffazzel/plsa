ACM Turing Centenary Celebration, ACM-TURING '12, San Francisco, California, USA, June 15-16, 2012.
John White
Vinton G. Cerf
Keith van Rijsbergen
Charles W. Bachman
Kelly Gotlieb
Wendy Hall
William Newman
Barbara J. Grosz
Edward A. Feigenbaum
Marvin Minsky
Judea Pearl
Raj Reddy
Butler W. Lampson
Dahlia Malkhi
Fernando J. Corbató
E. Allen Emerson
Joseph Sifakis
Ken Thompson
Alan C. Kay
Juris Hartmanis
Stephen Cook
William Kahan
Richard Edwin Stearns
Andrew C. Yao
Dana S. Scott
Edmund M. Clarke
David Patterson
Frederick P. Brooks Jr.
Ivan E. Sutherland
Charles P. Thacker
Susan Graham
Frances E. Allen
Barbara Liskov
Niklaus Wirth
Christos H. Papadimitriou
Leonard M. Adleman
Richard M. Karp
Donald E. Knuth
Robert E. Tarjan
Leslie G. Valiant
Vint Cerf
John E. Hopcroft
Robert E. Kahn
Ronald L. Rivest
Adi Shamir
