To Boldly Go Where No Human-Robot Team Has Gone Before, Papers from the 2006 AAAI Spring Symposium, Technical Report SS-06-07, Stanford, California, USA, March 27-29, 2006.
Terry Fong
Guy Hoffman
Cynthia Breazeal
Charles Lesire
Catherine Tessier
Geert-Jan M. Kruijff
Fang Tang
Lynne E. Parker
M. Bernardine Dias
Thomas K. Harris
Brett Browning
Edward Gil Jones
Brenna Argall
Manuela M. Veloso
Anthony Stentz
Alexander I. Rudnicky
Rachid Alami
Aurélie Clodic
Vincent Montreuil
Emrah Akin Sisbot
Raja Chatila
John Alan Atherton
Benjamin Hardin
Michael A. Goodrich
Maxime Ransan
Ella Atkins
Mark Yim
Nathan P. Koenig
Maja J. Mataric
James McLurkin
Jennifer Smith
James Frankel
David Sotkowitz
David Blau
Brian Schmidt
Gregory D. Hager
Allison M. Okamura
Russell H. Taylor
Vytas SunSpiral
Mark B. Allan
Rodney Martin
Kevin R. Wheeler
Dongjun Lee
Mark W. Spong
Matthew Dunbabin
Peter Corke
Graeme J. Winstanley
Jonathan M. Roberts
Alberto Elfes
John M. Dolan
Gregg Podnar
Sandra Mau
Marcel Bergerman
Liam Pedersen
William J. Clancey
Maarten Sierhuis
Nicola Muscettola
David E. Smith
David Lees
Kanna Rajan
Sailesh Ramakrishnan
Paul Tompkins
Alonso Vera
Tom Dayton
