Affective Computing and Intelligent Interaction, Third International Conference and Workshops, ACII 2009, Amsterdam, The Netherlands, September 10-12, 2009, Proceedings.
Qiang Ji
Jennifer L. Robison
Scott W. McQuiggan
James C. Lester
Panagiotis Petrantonakis
Leontios J. Hadjileontiadis
Egon L. van den Broek
Frans van der Sluis
Ton Dijkstra
Celso M. de Melo
Jonathan Gratch
Stefan Scherer
Volker Fritzsch
Friedhelm Schwenker
I. Wilson
Felix Burkhardt
Markus Van Ballegooy
Klaus-Peter Engelbrecht
Tim Polzehl
Joachim Stegmann
Dzmitry Tsetserukou
Alena Neviarouskaya
Helmut Prendinger
Naoki Kawakami
Susumu Tachi
Alexander Osherenko
Elisabeth André
Thurid Vogt
Manfred Klenner
Stefanos Petrakis
Angela Fahrni
Judith M. Kessens
Mark A. Neerincx
Rosemarijn Looije
M. Kroes
Gerrit Bloothooft
Mohammad Soleymani
J. Davis
Thierry Pun
Eva Hudlicka
J. Broekens
Roddy Cowie
Cian Doherty
E. McMahon
Michelle Karg
Robert Jenke
Wolfgang Seiberl
K. Kuuhnlenz
A. Schwirtz
Martin Buss
N. Dibben
Shazia Afzal
Tevfik Metin Sezgin
Yujian Gao
Peter Robinson
Alexis Héloir
Michael Kipp
Irene Lopatovska
D. Gokcay
S. Arikan
G. Yildirim
Laurel D. Riek
Tal-Chen Rabinowitch
Bhismadev Chakrabarti
Peter Robinson
Sérgio Hortas Rodrigues
Samuel Mascarenhas
João Dias
Ana Paiva
Elena Spiridon
Stephen H. Fairclough
K. Gottlicher
S. Stein
D. Reichardt
Christophe Vaudable
Laurence Devillers
C. Balague
Jean-Baptiste Dodane
Takatsugu Hirayama
Hiroaki Kawashima
Takashi Matsuyama
Alena Neviarouskaya
Helmut Prendinger
Mitsuru Ishizuka
Patrick Lucey
Jeffrey F. Cohn
Simon Lucey
Iain A. Matthews
Sridha Sridharan
Kenneth M. Prkachin
Chung-Yi Chi
Ying-Shian Wu
Wei-rong Chu
D. C. Wu
J. Y.-j. Hsu
R. T.-H. Tsai
Alexander Osherenko
Arttu Lämsä
Jani Mäntyjärvi
Alessandro Valitutti
Oliviero Stock
Carlo Strapparava
Jianhua Tao
Aijun Li
Shifeng Pan
Ling He
Margaret Lech
Namunu Chinthaka Maddage
Nicholas B. Allen
Joris H. Janssen
Egon L. van den Broek
Joyce H. D. M. Westerink
Simone Tognetti
C. Alessandro
Andrea Bonarini
Matteo Matteucci
Nicolas Rollet
Agnes Delaborde
Laurence Devillers
Georgios N. Yannakakis
Radoslaw Niewiadomski
Sylwia Julia Hyniewska
Catherine Pelachaud
Shaowen Bardzell
Jeffrey Bardzell
Tyler Pace
Nicolas Sabouret
Magalie Ochs
Micah Eckhardt
Rosalind W. Picard
Alexander Osherenko
Elisabeth André
Héctor Perez Martínez
Arnav Jhala
Georgios N. Yannakakis
Thurid Vogt
Elisabeth André
Johannes Wagner
Stephen W. Gilroy
Fred Charles
Marc Cavazza
Roderick Cowie
Gary McKeown
C. Gibney
César F. Pimentel
Maria R. Cravo
Egon L. van den Broek
Joris H. Janssen
Joyce H. D. M. Westerink
Jorge Peregrín Emparanza
Pavan Dadlani
Boris E. R. de Ruyter
Aki Härmä
T. R. Brick
M. D. Hunter
J. F. Cohn
Chengwei Huang
Yun Jin
Yan Zhao
Yinhua Yu
Li Zhao
Victor V. Kryssanov
Eric W. Cooper
Hitoshi Ogawa
I. Kurose
M. Iacobini
T. Gonsalves
Nadia Bianchi-Berthouze
Christopher D. Frith
Marjolein D. van der Zwaag
Joyce H. D. M. Westerink
Egon L. van den Broek
Emily Mower
Angeliki Metallinou
Chi-Chun Lee
Abe Kazemzadeh
Carlos Busso
Sungbok Lee
Shrikanth S. Narayanan
Sabrina Campano
Nicolas Sabouret
A. Kotov
Carlo Giovannella
Davide Conflitti
Riccardo Santoboni
Andrea Paoloni
Christian Becker-Asano
Toshiyuki Kanda
Carlos Toshinori Ishi
Hiroshi Ishiguro
Gary Garcia Molina
Tsvetomira Tsoneva
Anton Nijholt
Akshay Asthana
Jason M. Saragih
Michael Wagner
Roland Goecke
Florian Eyben
Martin Wöllmer
Björn W. Schuller
J. Bispo
Ana Paiva
Tibor Bosse
Edwin Zwanenburg
Judith M. Kessens
Mark A. Neerincx
Rosemarijn Looije
M. Kroes
Gerrit Bloothooft
Mitsuyo Hashida
S. Tanaka
Haruhiro Katayose
Joost Broekens
Willem-Paul Brinkman
Michael Kipp
Jean-Claude Martin
Katri Salminen
Jussi Rantala
Pauli Laitinen
Veikko Surakka
Jani Lylykangas
Roope Raisamo
Marc Schröder
Elisabetta Bevacqua
Florian Eyben
Hatice Gunes
Dirk Heylen
Mark ter Maat
Sathish Pammi
Maja Pantic
Catherine Pelachaud
Björn W. Schuller
Etienne de Sevin
Michel F. Valstar
Martin Wöllmer
Hana Boukricha
Ipke Wachsmuth
Andrea Hofstätter
Karl Grammer
Elena Vildjiounaite
Vesa Kyllönen
Olli Vuorinen
Satu-Marja Mäkelä
Tommi Keränen
Markus Niiranen
J. Knuutinen
Johannes Peltola
Joep J. M. Kierkels
Thierry Pun
Antonio Camurri
Giovanna Varni
Gualtiero Volpe
M. Al Masum Shaikh
Antonio Rui Ferreira Rebordão
Keikichi Hirose
Mitsuru Ishizuka
Xiaoying Xu
Aijun Li
Liping Hu
Jianhua Tao
Gert-Jan de Vries
Paul Lemmens
Dirk Brokken
Magalie Ochs
Nicolas Sabouret
Eva Hudlicka
Christian Becker-Asano
S. Payr
K. Fischer
R. Ventura
I. Leite
Christian von Scheve
Stefan Steidl
Anton Batliner
Björn W. Schuller
Dino Seppi
G. Laurans
P. Desmet
P. Hekkert
Miguel Bruns Alonso
David V. Keyson
Caroline Hummels
Dongdong Li
Yingchun Yang
Ting Huang
Mohammad Soleymani
Joep J. M. Kierkels
Guillaume Chanel
Thierry Pun
Lassi A. Liikkanen
Giulio Jacucci
Matti Helin
Stacy Marsella
Jonathan Gratch
Ning Wang
B. Stankovic
Cornelia Setz
Johannes Schumm
C. Lorenz
Bert Arnrich
Gerhard Tröster
Kristina Schaaff
Tanja Schultz
Stephen W. Gilroy
Marc Cavazza
Markus Niiranen
Elisabeth André
Thurid Vogt
Jérôme Urbain
Maurice Benayoun
Hartmut Seichter
Mark Billinghurst
N. Bee
S. Franke
E. Andre
Jonathan Gratch
Stacy Marsella
Ning Wang
B. Stankovic
M. Spitzer
S. de Waele
G.-J. de Vries
M. Jager
Dzmitry Tsetserukou
Alena Neviarouskaya
Suleman Shahid
Emiel Krahmer
Marc Swerts
Willem A. Melder
Mark A. Neerincx
V. T. Visch
M. B. Goudbeek
Dipankar Das
Sivaji Bandyopadhyay
N. Amir
A. Weiss
R. Hadad
Shazia Afzal
Peter Robinson
Alexis Clay
Nadine Couture
Laurence Nigay
Yunfeng Zhu
Fernando De la Torre
Jeffrey F. Cohn
Yu-Jin Zhang
J. A. Healey
Sathish Pammi
Marc Schröder
R. Benjamin Knapp
Javier Jaimovich
Niall Coghlan
Jeffrey F. Cohn
T. S. Kruez
Iain A. Matthews
Ying Yang
Minh Hoai Nguyen
M. T. Padilla
Feng Zhou
Fernando De la Torre
Jonghwa Kim
Elisabeth André
Thurid Vogt
Georgios N. Yannakakis
Zeynep Yücel
Albert Ali Salah
Miriam Madsen
Rana El Kaliouby
Micah Eckhardt
Matthew S. Goodwin
Mohammed E. Hoque
Rosalind W. Picard
Ginevra Castellano
Iolanda Leite
André Pereira
Carlos Martinho
Ana Paiva
Peter W. McOwan
Arjan Stuiver
Ben Mulder
Elisabeth Eichhorn
Reto Wettach
Boris Müller
Gordon McIntyre
Roland Göcke
M. Hyett
M. Green
Michael Breakspear
Jina Lee
Helmut Prendinger
Alena Neviarouskaya
S. Marsella
Diana Arellano
I. Lera
Javier Varona
Francisco J. Perales López
Joyce H. D. M. Westerink
Martin Ouwerkerk
Gert-Jan de Vries
S. de Waele
Jack van den Eerenbeemd
M. van Boven
Byung-Chull Bae
Robert Michael Young
Johannes Wagner
Elisabeth André
Frank Jung
Rob Duell
Zulfiqar A. Memon
Jan Treur
C. Natalie van der Wal
Elliott Bruce Hedman
Oliver Wilder-Smith
Matthew S. Goodwin
Ming-Zher Poh
R. Fletcher
Rosalind W. Picard
A. Das
S. Bandyopadhyay
E. Neuhaus-Klamer
P. Dadlani
Zhongzhe Xiao
Emmanuel Dellandréa
Liming Chen
Weibei Dou
Céline Clavel
Jean-Claude Martin
T. Gonsalves
Christopher D. Frith
Bruno B. Averbeck
Youssef Kashef
Abdelrahman N. Mahmoud
Rana El Kaliouby
Rosalind W. Picard
Nadia Bianchi-Berthouze
M. Iacobini
H. Critchley
H. Sloan
Steven Cadavid
Mohammad H. Mahoor
Daniel S. Messinger
Jeffrey F. Cohn
Ning Wang
Jonathan Gratch
Claire O'Bryne
Lola Cañamero
John Christopher Murray
Sander Koelstra
Christian Mühl
Ioannis Patras
Stephen H. Fairclough
Katie C. Ewing
Jenna Roberts
Konstantinos Bousmalis
Marc Mehu
Maja Pantic
Femke Nijboer
Stefan Carmien
Enrique Leon
Fabrice O. Morin
Randal A. Koene
Ulrich Hoffmann
M. Lehne
Klas Ihme
Anne-Marie Brouwer
Jan B. F. van Erp
Thorsten Oliver Zander
Katrin S. Lohan
Anna-Lisa Vollmer
Jannik Fritsch
Katharina J. Rohlfing
Britta Wrede
M. Al Masum Shaikh
Antonio Rui Ferreira Rebordão
A. Nakasone
P. Helmut
Keikichi Hirose
Dirk Heylen
Mariët Theune
Rieks op den Akker
Anton Nijholt
Thorsten Oliver Zander
Sabine Jatzev
Yi Li
Yiannis Aloimonos
Paul M. Brunet
Hastings Donnan
Gary McKeown
Ellen Douglas-Cowie
Roderick Cowie
Christian Mühl
Dirk Heylen
Céline Clavel
Albert Rilliard
Takaaki Shochi
Jean-Claude Martin
J. A. Zondag
Tommaso Gritti
Vincent Jeanne
Isabella Poggi
Francesca D'Errico
Alessandro Vinciarelli
Alfred Dielmann
Sarah Favre
Hugues Salamin
A. L. Arenas
Vicenç Gómez
Hilbert J. Kappen
