15th International Workshop on Database and Expert Systems Applications (DEXA 2004), with CD-ROM, 30 August - 3 September 2004, Zaragoza, Spain.
Tsunenori Ishioka
Masayuki Kameda
Monica Palmirani
Raffaella Brighi
Matteo Massini
Nikolaos Nanas
Victoria S. Uren
Anne N. De Roeck
Marta Gatius
Manuel Bertrán
Horacio Rodríguez
Estela Saquete
Patricio Martínez-Barco
Rafael Muñoz
Stéphane Bressan
Riky Irawan
Carlos Periñán-Pascual
Francisco Arcas-Túnez
Rolf Schwitter
Lorna Hards
Veronica Sekules
Vito Cappellini
Giovanna Damiani
Alessandro Piva
Tim Redfern
Eoin Kilfeather
Franca Garzotto
Paolo Paolini
Marco Speroni
Birgit Pröll
Werner Retschitzegger
Wieland Schwinger
Trevor D. Collins
Paul Mulholland
Zdenek Zdráhal
Paul Mulholland
Zdenek Zdráhal
Trevor D. Collins
Jan Uhlír
Martin Falc
Irene Lourdi
Christos Papatheodorou
Petr Aubrecht
Lubos Kral
Sebastiano Colazzo
Vito Perrone
Kamil Matousek
Jan Uhlír
Eva Gahleitner
Wolfram Wöß
Raffaella Brighi
Wallace Anacleto Pinheiro
Ana Maria de Carvalho Moura
José A. Alonso-Jiménez
Joaquín Borrego-Díaz
Antonia M. Chávez-González
Silvana Castano
Alfio Ferrara
Stefano Montanelli
Gianpaolo Racca
Devis Bianchini
Valeria De Antonellis
Michele Melchiori
Cláudio de Souza Baptista
Karine F. Vasconcelos
Ladjane S. Arruda
Ulrich Schiel
Elvis Rodrigues da Silva
Ralf Klischewski
Barbara Carminati
Elena Ferrari
Bhavani M. Thuraisingham
Torsten Priebe
Christian Schläger
Günther Pernul
Randy Howard
Larry Kerschberg
Carmen Costilla
Juan P. Palacios
María José Rodríguez
José Cremades
Antonio Calleja
Raúl Fernández
Jorge Vila
Radek Burget
Dean Williams
Alexandra Poulovassilis
Adriana J. Berlanga
Francisco J. García-Peñalvo
S. Misbah Deen
Michel Sala
Pierre Pompidor
Danièle Hérin
Gaël Isoird
Thomas Biskup
Jorge Marx Gómez
Martin Bernauer
Gerti Kappel
Elke Michlmayr
Thang To
Dan J. Smith
Ismael Navas Delgado
Nathalie Moreno Vergara
Antonio C. Gomez Lora
María del Mar Roldán García
Iván Ruiz Mostazo
José Francisco Aldana Montes
Stephen Sobol
Jennie Roux
Jordi Bataller
Hendrik Decker
Luis Irún-Briz
Francesc D. Muñoz-Escoí
Rainer Schmidt
Victor M. Ruiz Penichet
José A. Gallud
M. L. González
Pascual González
Willy Picard
Alejandro Fernández
Marek Suchocki
Jerome Robinson
Mahmoud Brahimi
Mahmoud Boufaïda
Lionel Seinturier
Waldemar Wieczerzycki
Nobuyoshi Sato
Minoru Uehara
Kaoru Sugita
Tomoyuki Ishida
Akihiro Miyakawa
Yoshitaka Shibata
Akio Koyama
Tadahiko Abiko
Noriyuki Kamibayashi
Norio Narita
Daisuke Hironaka
Masao Yokota
Leonard Barolli
Yoshitaka Honma
Akio Koyama
Arjan Durresi
Junpei Arai
Yosuke Nakamura
Xuejun Tian
Tetsuo Ideguchi
Takashi Okuda
Kenichi Watanabe
Tomoya Enokido
Makoto Takizawa
Satoshi Itaya
Tomoya Enokido
Makoto Takizawa
Akihiro Miyakawa
Tomoyuki Ishida
Kaoru Sugita
Yoshitaka Shibata
Yuka Kato
DongMei Jiang
Katsuya Hakozaki
Yoshiki Murotani
Minoru Uehara
Hideki Mori
Yang Wang
Osmar R. Zaïane
Randy Goebel
Jennafer L. Southron
Urmila Basu
Randy M. Whittal
Julie L. Stephens
Gregory J. Taylor
Zoé Lacroix
Kaushal Parekh
Maria-Esther Vidal
Hasan Davulcu
Zoé Lacroix
Kaushal Parekh
I. V. Ramakrishnan
Nikeeta Julasana
Michael Maibaum
Galia Rimon
Christine A. Orengo
Nigel J. Martin
Alexandra Poulovassilis
Ilya Zaslavsky
Haiyun He
Joshua Tran
Maryann E. Martone
Amarnath Gupta
Jianting Zhang
Le Gruenwald
Margherita Berardi
Michele Lapi
Pietro Leo
Donato Malerba
Caterina Marinelli
Gaetano Scioscia
Alex G. Büchner
David W. Patterson
Daniela Leal Musa
Lydia Silva Muñoz
José Palazzo Moreira de Oliveira
Rachid Anane
S. Crowther
J. Beadle
Georgios K. Theodoropoulos
Abdulrahman A. Mirza
Amala Vijaya Selvi Rajan
Jim Otieno
D. Burnell
Ala Al-Zobaidie
G. Windall
A. Butler
Simone Stumpf
Janet McDonnell
Diane Mularz
Margaret Lyell
Wilfried Njomgue Sado
Dominique Fontaine
Philippe Fontaine
A. Riki Y. Morikawa
Larry Kerschberg
Jan Aidemark
Robert Woitsch
Dimitris Karagiannis
Peter Höfferer
Bernhard Teuchmann
Alfs T. Berztiss
Jörg Becker
Lev Vilkov
Christian Brelage
Aurora Vizcaíno
Francisco Ruiz
Mario Piattini
Félix García
Kemal A. Delic
Laurent Douillet
Nicolae Tandareanu
Mihaela Ghindeanu
Anastasios Gounaris
Norman W. Paton
Rizos Sakellariou
Alvaro A. A. Fernandes
Samir Jafar
Sébastien Varrette
Jean-Louis Roch
Marko Niinimäki
John White
Wim Som de Cerff
Joni Hahkala
Tapio Niemi
Mikko Pitkänen
Zoran Majkic
Silvana Castano
Alfio Ferrara
Stefano Montanelli
Elena Pagani
Gian Paolo Rossi
Stefano Tebaldi
Shahram Ghandeharizadeh
Bhaskar Krishnamachari
Marcel Karnstedt
Katja Hose
Kai-Uwe Sattler
Jean-Paul Arcangeli
Sébastien Leriche
Marc Pantel
Bernhard Fiser
Umut Onan
Ibrahim Elsayed
Peter Brezany
A Min Tjoa
Jean-Marc Pierson
Ludwig Seitz
Hector Duque
Johan Montagnat
Tao-Yuan Jen
Rafik Taouil
Dominique Laurent
Ronald E. McRoberts
Krista M. Gebert
Patrick D. Miles
Greg C. Liknes
R. James Barbour
Patrick D. Miles
Wayne D. Shepperd
Ronald E. McRoberts
John S. Vissage
Kenneth E. Skog
Bryce J. Stokes
Binh Thanh Nguyen
Mohamed T. Ibrahim
Chiyaba Njovu
Mohammed T. Ibrahim
Kohyu Satoh
Song Weiguo
K. T. Yang
Paolo Fiorucci
Francesco Gaetani
Riccardo Minciardi
Roberto Sacile
Eva Trasforini
Keith Rennolls
Tim Richards
Alexander M. Fedorec
Mohamed T. Ibrahim
Kevin McManus
Alun Butler
Keith Rennolls
Tim Richards
Alexander M. Fedorec
Mohamed T. Ibrahim
Kevin McManus
Alun Butler
Riccardo Minciardi
Michela Robba
Roberto Sacile
Alan Thomson
Mandy Haggith
Ravi Prabhu
Gary Hoi Kit Lam
Hong Va Leong
Stephen Chi-fai Chan
Xiulan Yu
Ying Chen
Fangyan Rao
Dong Liu
Sergio Ilarri
Eduardo Mena
Arantza Illarramendi
Ricky Robinson
Jadwiga Indulska
Nikola Mitrovic
José A. Royo
Eduardo Mena
Vladimir Zadorozhny
Panos K. Chrysanthis
Alexandros Labrinidis
Christophe Bobineau
Cyril Labbé
Claudia Roncancio
Patricia Serrano-Alvarado
Shlomi Dolev
Reuven Yagel
Helene N. Lim Choi Keung
Justin R. D. Dyson
Stephen A. Jarvis
Graham R. Nudd
Markus C. Huebscher
Julie A. McCann
Jim Dowling
Raymond Cunningham
Eoin Curran
Vinny Cahill
Aris A. Papadopoulos
Julie A. McCann
Gawesh Jawaheer
Julie A. McCann
Alun Butler
Mohamed T. Ibrahim
Keith Rennolls
Liz Bacon
Mohamed T. Ibrahim
Ric Telford
Petre Dini
Pascal Lorenz
Nino Vidovic
Richard Anthony
Richard John Anthony
Roy Sterritt
David F. Bantz
Christine Draper
Randy George
Marcello Vitaletti
Andrew Lee
N. Badr
A. Taleb-Bendiab
Martin Randles
D. Reilly
Stefan Paal
Reiner Kammüller
Bernd Freisleben
Rem W. Collier
Michael J. O'Grady
Gregory M. P. O'Hare
Conor Muldoon
Donnacha Phelan
Robin Strahan
Yanjun Tong
Petre Dini
Cosmin Dini
Manuela Popescu
Andrew Harvey
Mujiba Zaman
Jyotsna Surabattula
Le Gruenwald
Petre Dini
Wolfgang Gentzsch
Mark Potts
Alexander Clemm
Mazin S. Yousif
Andreas Polze
Toni Navarrete
Josep Blat
Maurici Ruiz
Rob Lemmens
Helbert Arenas
Stefano Nativi
John Caron
Ben Domenico
Anselmo Cardoso de Paiva
Elvis Rodrigues da Silva
Fábio Luiz Leite Jr.
Cláudio de Souza Baptista
Lionel Savary
Tao Wan
Karine Zeitouni
J. Francisco Ramos
Miguel Chover
Carlos Granell
José Poveda
Michael Gould
Kris Kolodziej
José Danado
Carlos Granell
J. Francisco Ramos
Udo Einspanier
Lorenzo Bigagli
Stefano Nativi
Paolo Mazzetti
G. Villoresi
Ludger Becker
Tobias Gerke
Klaus H. Hinrichs
Tina Strauf née Hausmann
Jan Vahrenhold
Patricia Hartnett
Michela Bertolotto
Riccardo Albertoni
Alessio Bertone
Monica De Martino
Olaf Nölle
José Eduardo Córcoles
Pascual González
Javier Nogueras-Iso
F. J. Zarazaga
Pedro R. Muro-Medrano
Angi Voß
Vera Hernández Ernst
Hans Voß
Simon Scheider
Thierry Badard
Arnaud Braun
Gabriella Pasi
Guy De Tré
Rita M. M. De Caluwe
Janusz R. Getta
Patrick Bosc
Olivier Pivert
José Galindo
Angélica Urrutia
Mario Piattini
Ander de Keijzer
Maurice van Keulen
Sara Comai
Stefania Marrara
Letizia Tanca
Matteo Magnani
Danilo Montesi
Ed Porto Bezerra
Ulrich Schiel
Bernardo Lula Jr.
Arta Dilo
Pawalai Kraipeerapun
Wim Bakker
Rolf A. de By
Ouri Wolfson
Bo Xu
Daniel Lübke
Jorge Marx Gómez
Terry Bearly
Vijay Kumar
Abhilash Gummadi
Jong P. Yoon
Shalendra Chhabra
Ernesto Damiani
Sabrina De Capitani di Vimercati
Stefano Paraboschi
Pierangela Samarati
Taizo Yamada
Kenro Aihara
Atsuhiro Takasu
Jun Adachi
Dietrich Fahrenholtz
Volker Turau
Lex Wedemeijer
E. G. A. J. de Bruin
Tatyana Podgayetskaya
Wolffried Stucky
Werner Aigner
Peter Regner
Thomas Wiesinger
Josef Küng
Marcelo Trannin Machado
Marcos R. S. Borges
Renata Mendes de Araujo
Damien Bright
Gerald Quirchmayr
Andreas Wombacher
Karl Aberer
