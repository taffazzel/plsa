10th International Workshop on Database & Expert Systems Applications, Florence, Italy, September 1-3, 1999, Proceedings.
Tadeusz Morzy
Zbyszko Królikowski
Janusz R. Getta
Seyed M. Sedighi
László Böszörményi
Robert Eisner
Herbert Groiss
Ahmed Mostefaoui
Lionel Brunie
Gil Seong Na
Sang Ho Lee
Sergio Carvalho
Alberto Lerner
Sérgio Lifschitz
Jens Albrecht
Wolfgang Sporer
Andreas Rauber
Philipp Tomsich
Peter Triantafillou
Ioannis Georgiadis
Sandra de F. Mendes Sampaio
Norman W. Paton
Paul Watson
Jim Smith
Florian Waas
Faïza Najjar
Yahya Slimani
Shirish Hemant Phatak
B. R. Badrinath
Subhasish Mazumdar
Panos K. Chrysanthis
Andrzej Cichocki
Marek Rusinkiewicz
David Ratner
Peter L. Reiher
Gerald J. Popek
Lars Frank
Ravi Prakash
Roberto Baldoni
Ana Paula Afonso
Francisco S. Regateiro
Mário J. Silva
Constantinos Spyrou
George Samaras
Evaggelia Pitoura
Paraskevas Evripidou
Andry Rakotonirainy
Roger Weber
Pavel Zezula
Vladimir Pestov
Raffaele Cappelli
Dario Maio
Davide Maltoni
Zbigniew R. Struzik
Arno Siebes
Stefania Ardizzoni
Ilaria Bartolini
Marco Patella
Apostolos Papadopoulos
Yannis Manolopoulos
Romaric Besançon
Martin Rajman
Jean-Cédric Chappelier
G. Bianco
Valeria De Antonellis
Silvana Castano
Michele Melchiori
Claudia Diamantini
Maurizio Panti
Paolo Ciaccia
Marco Patella
Christian Zirkelbach
Pavel Zezula
Pasquale Savino
Paolo Ciaccia
Fausto Rabitti
Seppo Puuronen
Vagan Y. Terziyan
Alexander Logvinovsky
Paolo Avesani
Enrico Blanzieri
Francesco Ricci
Hiroshi Asakura
Masatoshi Yoshikawa
Shunsuke Uemura
Danilo Montesi
Alberto Trombetta
Mao Lin Huang
Wei Lai
Yanchun Zhang
Chris Pilgrim
Ying K. Leung
John Cugini
Jean Scholtz
Thomas M. Mann
Jayesh Govindarajan
Matthew O. Ward
H. Hörtner
E. Berger
M. A. M. S. Oliveira
Glauco Todesco
Regina Borges de Araujo
Andreas Rauber
Harald Bina
Flavio Rossi
Michela Spagnuolo
Peter J. Funk
Ivica Crnkovic
Jolita Ralyté
Mounia Fredj
Ounsa Roudiès
Bernhard Deifel
Vito Veneziano
Sara Jones
Carol Britton
Javier Andrade Garda
Juan Ares Casal
Óscar Dieste Tubío
Rafael García Vázquez
Marta López Fernández
Santiago Rodríguez Yánez
Luisa Verde
Alessandro Maccari
Ian F. Alexander
Mustapha Tawbi
Camille Ben Achour
Fernando Vélez
Neil A. M. Maiden
G. Rugg
P. Patel
Suzanne Robertson
James Robertson
Bashar Nuseibeh
Steve M. Easterbrook
Bjarne Tvete
Dermot Hore
Vesa Torvinen
Helen Sharp
Anthony Finkelstein
Galal Galal
Sudeep Mallick
S. Krishna
Donna Peuquet
Marius Thériault
Anne-Marie Séguin
Yanick Aubé
Paul Y. Villeneuve
C. de Zeeuw
Arnold Bregt
M. Sonneveld
J. van den Brink
Jean-Marc Saglio
José Moreira
John F. Roddick
Fabio Grandi
Federica Mandreoli
Maria Rita Scalas
Martin Erwig
Markus Schneider
Timos K. Sellis
Viviane Pereira Moreira
Nina Edelweiss
Rosanne Price
Kotagiri Ramamohanarao
Bala Srinivasan
Marie-Hélène de Sède
Alexandre Moine
Patrick Marceau
Laurent Spéry
Christophe Claramunt
Thérèse Libourel
Marie-Christine Fauvet
S. Chardonnel
Marlon Dumas
Pierre-Claude Scholl
P. Dumolard
Alessandro Artale
Enrico Franconi
Kathleen Hornsby
Max J. Egenhofer
Paolo Terenziani
Paolo Mancarella
Alessandra Raffaetà
Franco Turini
Fosca Giannotti
Robert Jeansoulin
Yannis Theodoridis
Javier Bazzocco
Gisela Trilla
Silvia E. Gordillo
Sima Asgari
Naoki Yonezaki
Abdel Belaïd
Amos David
Xien Fan
Fang Sheng
Peter A. Ng
Stefano Messelodi
Carla Maria Modena
Laurence Likforman-Sulem
Bibiana Cuenca
Toshio Kawashima
Tadakatu Nakagawa
Takashi Miyazaki
Yoshinao Aoki
Serena La Manna
Anna Maria Colla
Alessandro Sperduti
Jianying Hu
Ramanujan S. Kashi
Gordon T. Wilfong
Jonathan J. Hull
Dar-Shyang Lee
John F. Cullen
Peter E. Hart
Alexander F. Gelbukh
Grigori Sidorov
Adolfo Guzmán-Arenas
Jerh. O'Connor
Alan F. Smeaton
Ulrich Schiel
Ianna M. S. F. de Sousa
Edberto Ferneda
Sun-Hwa Hahn
Joon Ho Lee
Jin Hyung Kim
Enrico Appiani
L. Boato
S. Bruzzo
Anna Maria Colla
M. Davite
Donatella Sciarra
Frédérique Laforest
Anne Tchounikine
Pietro Parodi
Rodolfo Brancaleon
F. Venuti
G. Musso
Vincent Torre
Hans-Arno Jacobsen
Gerrit Riessen
Oliver Günther
Rudolf Müller
Manolis Marazakis
Dimitris Papadakis
Christos Nikolaou
Penelope Constanta
Martin Bichler
Marion Kaukal
Avigdor Gal
Danilo Montesi
Duen-Ren Liu
Shien-Chang Pan
Kok-Leong Ong
C. Chew
Wee Keong Ng
Ee-Peng Lim
Colin C. Charlton
Janet Little
R. K. Lloyd
Simon Morris
Irene Neilson
Athena Vakali
Wassili Kazakos
Ralf Kramer
Ralf Nikolai
Dimitri Nikolaidis
Koji Hashimoto
Yoshitaka Shibata
Norio Shiratori
David De Roure
Nigel Walker
Les Carr
Junichi Suzuki
Yoshikazu Yamamoto
Hiroaki Higaki
Katsuya Tanaka
Makoto Takizawa
Chu-Sing Yang
Jiing-Ching Yang
Kun-da Wu
Jian-Xing Lee
Yuh-Rong Leu
Martyn Fletcher
S. Misbah Deen
Chaitanya K. Baru
Costas Petrou
Stathes Hadjiefthymiades
Drakoulis Martakos
Dell Zhang
Yisheng Dong
A.-K. Luah
Wee Keong Ng
Ee-Peng Lim
W.-P. Lee
Yinyan Cao
Wolfgang May
James C. French
Allison L. Powell
Vinod Anupam
Yuri Breitbart
Juliana Freire
Bharat Kumar
Christophe Gnaho
François Larcher
Pallavi Priyadarshini
Fengqiong Qin
Ee-Peng Lim
Wee Keong Ng
Thomas Füssl
Heike Utermann
Jack R. Brzezinski
George J. Knafl
Alain Bouju
Arunas Stockus
Frédéric Bertrand
Patrice Boursier
Mohamed T. Elhadi
Tibor Vamos
Trevor J. M. Bench-Capon
Jos Lehmann
Mario Ragona
Loriana Serrotti
Fiorenza Socci
Pier-Luigi Spinosa
Antonio Cammelli
Elio Fameli
Erich Schweighofer
Dieter Merkl
Rolf Oppliger
Ralph Holbein
Thomas Gaugler
Alexander Runge
Bernd Schopp
Katarina Stanoevska-Slabeva
Ioannis Mavridis
George Pangalos
Triadafyllos Koukouvinos
Sead Muftic
Eduardo B. Fernández
Khanh Quoc Nguyen
Vijay Varadharajan
Yi Mu
Sigrid Gürgens
Javier Lopez
René Peralta
Feng Bao
Robert H. Deng
Khanh Quoc Nguyen
Vijay Varadharajan
C.-C. Liew
Wee Keong Ng
Ee-Peng Lim
B.-S. Tan
Kok-Leong Ong
Vassilis Prevelakis
Jean-Henry Morin
Dimitri Konstantas
Tetsuji Takada
Hideki Koike
Mi Hee Yoon
Yong-Ik Yoon
Kio Chung Kim
Sungchae Lim
Myoung-Ho Kim
Franco Bartolini
Vito Cappellini
Alessandro Piva
A. Fringuelli
Mauro Barni
Santosh Kulkarni
Bala Srinivasan
M. V. Ramakrishna
Christos Bouras
Apostolos Gkamas
Thrasyvoulos Tsiatsos
Didier Le Tien
Olivier Villin
Christian Bac
Hiroshi Ishikawa
Kazumi Kubota
Yasuhiko Kanemasa
Yasuo Noguchi
Haiwei Ye
Brigitte Kerhervé
Gregor von Bochmann
Ge Yu
Guoren Wang
Kunihiko Kaneko
Akifumi Makinouchi
