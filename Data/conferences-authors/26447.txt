Logical Formalizations of Commonsense Reasoning, Papers from the 2007 AAAI Spring Symposium, Technical Report SS-07-05, Stanford, California, USA, March 26-28, 2007.
Michael L. Anderson
Matthew D. Schmill
Tim Oates
Donald Perlis
Darsana P. Josyula
Dean Wright
Shomir Wilson
Jorge A. Baier
Sheila A. McIlraith
Marcello Balduccini
Alexander Bochman
Patrick Caldon
Eric Martin
Sandeep Chintabathina
Michael Gelfond
Richard Watson
Stefania Costantini
Arianna Tocchio
Pierangelo Dell'Acqua
Alfredo Gabaldon
Hojjat Ghaderi
Hector J. Levesque
Yves Lespérance
Patrick Hayes
Aaron Hunter
James P. Delgrande
Benjamin Johnston
Mary-Anne Williams
Michael Kandefer
Stuart C. Shapiro
Vladimir Lifschitz
Wanwan Ren
Fangzhen Lin
Martin Magnusson
Patrick Doherty
David Mallenby
Loizos Michael
Fabrizio Morbini
Lenhart K. Schubert
Leora Morgenstern
Erik T. Mueller
Rutu Mulkar
Jerry R. Hobbs
Eduard H. Hovy
Deepak Ramachandran
Paulo E. Santos
Pedro Cabalar
Dafna Shahaf
Eyal Amir
Richmond H. Thomason
Anbu Yue
Zuoquan Lin
