18th International Workshop on Database and Expert Systems Applications (DEXA 2007), 3-7 September 2007, Regensburg, Germany.
Toshiyuki Amagasa
Kentarou Kido
Hiroyuki Kitagawa
Fabio Fassetti
Bettina Fazzinga
Daniel Fötsch
Angela Bonifati
Alfredo Cuzzocrea
Massimo Franceschet
Angelo Montanari
Donatella Gubiani
Sven Hartmann
Sebastian Link
Stéphane Gançarski
Cécile Le Pape
Alda Lopes Gançarski
Hajime Takekawa
Hiroshi Ishikawa
Costantino Grana
Daniele Borghesani
Rita Cucchiara
Hassan A. Kingravi
M. Emre Celebi
Pragya P. Rajauria
Mohammadreza Keyvanpour
Shabnam Asbaghi
Mahmood Fathy
Miguel Rodríguez Luaces
José R. Paramá
Oscar Pedreira
Diego Seco
Jose Ramon Rios Viqueira
Nieves R. Brisaboa
Yolanda Cillero
Antonio Fariña
Susana Ladra
Oscar Pedreira
Antonio Penta
Antonio Picariello
Letizia Tanca
Petr Chmelar
Jaroslav Zendulka
Giovanni Frattini
Francesco Gaudino
Vladimiro Scotto di Carlo
Joan De Boeck
Kristof Verpoorten
Kris Luyten
Karin Coninx
Maurizio Montagnuolo
Alberto Messina
Augusto Celentano
Fabio Pittarello
Claudio Cerullo
Marco Porta
Maria Chiara Caschera
Arianna D'Ulizia
Václav Snásel
Pavel Krömer
Suhail S. J. Owais
Dusan Húsek
Behzad Moshiri
Amir Keyhanipour
Suhail S. J. Owais
Pavel Krömer
Václav Snásel
Saeedeh Maleki-Dizaji
Henry O. Nyongesa
Pavel Krömer
Václav Snásel
Jan Platos
Suhail S. J. Owais
Petra Kudová
Jiri Dvorský
Antonia Azzini
Stefania Marrara
Ivan Zelinka
Pavel Varacha
Roman Neruda
Stanislav Slusny
Ivan Zelinka
Roman Senkerik
Eduard Navratil
Zuzana Oplatková
Ivan Zelinka
Elena Baralis
Tania Cerquitelli
Vincenzo D'Elia
Duckjin Chai
Long Jin
Buhyun Hwang
Keun Ho Ryu
Li-Qing Qiu
Bin Pang
Sai-Qun Lin
Peng Chen
Zhenxing Luo
NuerMaimaiti Heilili
Zuoquan Lin
Giovanni Maria Sacco
Yannis Tzitzikas
Anastasia Analyti
Sébastien Ferré
Olivier Ridoux
Moritz Stefaner
Boris Müller
Barbara Demo
Alessio Angius
Gerhard Rolletschek
Efstathios Stamatatos
Malcolm Clark
Stuart N. K. Watt
Wim De Smet
Marie-Francine Moens
Carmine Cesarano
Antonino Mazzeo
Antonio Picariello
Sven Meyer zu Eissen
Benno Stein
Shengli Wu
Yaxin Bi
Sally I. McClean
Hitoshi Nakakubo
Shinsuke Nakajima
Kenji Hatano
Jun Miyazaki
Shunsuke Uemura
Li Cun-he
Lv Ke-qiang
Hyunzoo Chai
Mathias Lux
Michael Granitzer
Roman Kern
Christian Wartena
Rogier Brussee
Luit Gazendam
Willem-Olaf Huijsen
Lars Schubert
Martin Kuttner
Bernd Frankenstein
Dieter Hentschel
Prakash Kripakaran
Sandro Saitta
Suraj Ravindran
Ian F. C. Smith
Constanze Tschöpe
Matthias Wolff
Ernst Forstner
Reinhard Stumptner
Bernhard Freudenthaler
Josef Küng
Alan Eckhardt
Jaroslav Pokorný
Peter Vojtás
Daisuke Kitayama
Youko Kitahata
Ryoko Hiramoto
Kazutoshi Sumiya
Kittisak Kerdprasop
Nittaya Kerdprasop
Apichai Ritthongchailert
Nguyen Anh Thy Tran
Tran Khanh Dang
Guy De Tré
Tom Matthé
Parisa Kordjamshidi
Marysa Demoor
Patrick Bosc
Allel HadjAli
Hélène Jaudoin
Olivier Pivert
Marlene Goncalves
Leonid Tineo
Hendrik Decker
Davide Martinenghi
Pablo Garcia Bringas
Xuan Tian
Xiaoyong Du
Haihua Li
Ander de Keijzer
Maurice van Keulen
Marco Pagani
Gloria Bordogna
Massimiliano Valle
Giulia Bruno
Paolo Garza
Elisa Quintarelli
Rosalba Rossato
Mahboob Alam Khalid
Valentin Jijkoun
Maarten de Rijke
Murat Koyuncu
Giuseppe Pirrò
Domenico Talia
Domenico Beneventano
Sabina El Haoum
Daniele Montanari
Athanasios Bouras
Panagiotis Gouvas
Gregoris Mentzas
Carlo Curino
Giorgio Orsi
Letizia Tanca
Federica Mandreoli
Antonio Massimiliano Perdichizzi
Wilma Penzo
Alexandra Galatescu
Cristian Neicu
Taisia Greceanu
Stefano Paolozzi
Paolo Atzeni
Andreas Langegger
Martin Blöchl
Wolfram Wöß
Johan Petrini
Tore Risch
Jos de Bruijn
Stijn Heymans
Alessio Bosca
Dario Bonino
Yoo Jung An
James Geller
Yi-Ta Wu
Soon Ae Chun
Amjad Abou Assali
Dominique Lenne
Bruno Debray
Silvana Castano
Alfio Ferrara
Javier Tejedor
Roberto García
Miriam Fernández
Fernando J. López-Colino
Ferran Perdrix
José A. Macías
Rosa M. Gil
Marta Oliva
Diego Moya
José Colás
Pablo Castells
Simon Scerri
Brian Davis
Siegfried Handschuh
José L. Balcázar
Albert Bifet
Antoni Lozano
Tarek Hamrouni
Sadok Ben Yahia
Engelbert Mephu Nguifo
Wassim Ayadi
Khedija Arour
Ines Bouzouita
Samir Elloumi
Aurélie Bertaux
Agnès Braud
Florence Le Ber
Sami Zghal
Engelbert Mephu Nguifo
Karim Kamoun
Sadok Ben Yahia
Yahya Slimani
Sabine Bruaux
Gilles Kassel
Gilles Morel
Géraldine Polaillon
Marie-Aude Aufaure
Bénédicte Le Grand
Michel Soto
Olivier Bedel
Sébastien Ferré
Olivier Ridoux
Erwan Quesseveur
Thomas Tilley
Peter W. Eklund
Mi Sug Gu
Jeong Hee Hwang
Keun Ho Ryu
António Leong
Simon Fong
Zhuang Yan
Ling Wang
Jinli Cao
Sean Thompson
Torab Torabi
Andreas Bischoff
MinHwak Ok
Yang-Soo Lee
Sun-Ho Hong
Tomohisa Yamashita
Daisuke Takaoka
Noriaki Izumi
Akio Sashima
Koichi Kurumatani
Kôiti Hasida
Cecilia Challiol
Andres Fortier
Silvia E. Gordillo
Gustavo Rossi
Saqib Ali
Torab Torabi
Ben Soh
Zhuang Yan
Francis Yan
Simon Fong
Ilias Michalarias
Arkadiy Omelchenko
Jaydip Sen
M. Girish Chandra
P. Balamuralidhar
Harihara S. G.
Harish Reddy
Senol Zafer Erdogan
Sajid Hussain
Y. Mohamadi Begum
M. A. Maluk Mohamed
Carolina González
Juan C. Burguillo-Rial
Martín Llamas Nistal
Sara Comai
Juan Carlos Preciado
Marino Linaje Trigueros
Rober Morales-Chaparro
Fernando Sánchez-Figueroa
Mihaela Brut
Francesco Di Cerbo
Giancarlo Succi
Eric K. W. Lau
Emanuela Falcinelli
Candida Gori
Judit Jassó
Alfredo Milani
Simonetta Pallottelli
Emanuela Falcinelli
Floriana Falcinelli
Chiara Laici
Alfredo Milani
Giustina Secundo
Gianluca Elia
Cesare Taurino
Falk Huettmann
Hao Wang
Yuyi Ou
Jie Ling
Xiang Xu
Heqing Guo
Jesús Téllez Isaac
José Sierra Camara
Hyun Seok Kim
Jin-Young Choi
Mildrey Carbonell
José María Sierra
Joaquín Torres Márquez
Antonio Izquierdo
Serena Pastore
Luis Zarza
Josep Pegueroles
Miguel Soriano
Diego Suarez
Joaquín Torres Márquez
Mildrey Carbonell
Jesús Téllez Isaac
Brian Shields
Owen Molloy
Philip Robinson
Denis Hatebur
Maritta Heisel
Holger Schmidt
Francisco Sánchez-Cid
Antonio Mana
Dan Thomsen
Eduardo B. Fernández
Preethi Cholmondeley
Olaf Zimmermann
Roland Erber
Christian Schläger
Günther Pernul
Taufiq Rochaeli
Claudia Eckert
Sigrid Gürgens
Carsten Rudolph
Antonio Maña
Antonio Muñoz
Dirk Kotze
Martin S. Olivier
Esther Palomar
Juan M. Estévez-Tapiador
Julio César Hernández Castro
Arturo Ribagorda
Yoshio Nakajima
Kenichi Watanabe
Alireza Goudarzi Nemati
Tomoya Enokido
Makoto Takizawa
Somchai Chatvichienchai
Katsumi Tanaka
Marcin Marczewski
Tadeusz Pankowski
Ladjel Bellatreche
Guy Pierra
Mouna Kacimi
Kokou Yétongnon
Anirban Mondal
Sanjay Kumar Madria
Masaru Kitsuregawa
Mario Bisignano
Giuseppe Di Modica
Orazio Tomarchio
Lorenzo Vita
Ladjel Bellatreche
Birgit Korherr
Beate List
Abdul Babar
Karl Cox
Vladimir Tosic
Steven J. Bleistein
June M. Verner
Marielba Zacarias
Artur Caetano
R. Magalhaes
Helena Sofia Pinto
José M. Tribolet
Christian Wolter
Henrik Plate
Cédric Hébert
Sandeep Purao
Sharoda Paul
Steven Smith
Peter Hrastnik
Werner Winiwarter
