Proceedings of the 51st Annual Meeting of the Association for Computational Linguistics, ACL 2013, 4-9 August 2013, Sofia, Bulgaria, Volume 1: Long Papers.
Yang Liu
Kun Wang
Chengqing Zong
Keh-Yih Su
Thomas Schoenemann
Trevor Cohn
Lucia Specia
Brian Roark
Cyril Allauzen
Michael Riley
Haonan Yu
Jeffrey Mark Siskind
Mehdi Manshadi
Daniel Gildea
James F. Allen
Qi Li
Heng Ji
Liang Huang
Gabor Angeli
Jakob Uszkoreit
Camille Guinaudeau
Michael Strube
Elijah Mayfield
David Adamson
Carolyn Penstein Rosé
Greg Durrett
David Leo Wright Hall
Dan Klein
Meishan Zhang
Yue Zhang
Wanxiang Che
Ting Liu
Francesco Sartorio
Giorgio Satta
Joakim Nivre
Matthias Büchse
Alexander Koller
Heiko Vogler
Isao Goto
Masao Utiyama
Eiichiro Sumita
Akihiro Tamura
Sadao Kurohashi
Nan Yang
Shujie Liu
Mu Li
Ming Zhou
Nenghai Yu
Wang Ling
Guang Xiang
Chris Dyer
Alan W. Black
Isabel Trancoso
Jun Zhu
Xun Zheng
Bo Zhang
Miguel B. Almeida
André F. T. Martins
Taylor Berg-Kirkpatrick
Greg Durrett
Dan Klein
Joohyun Kim
Raymond J. Mooney
Omri Abend
Ari Rappoport
Weiwei Guo
Hao Li
Heng Ji
Mona T. Diab
Cristian Danescu-Niculescu-Mizil
Moritz Sudhof
Dan Jurafsky
Jure Leskovec
Christopher Potts
Isaac Persing
Vincent Ng
Camillo Lugaresi
Barbara Di Eugenio
David Marecek
Milan Straka
Yuan Zhang
Regina Barzilay
Amir Globerson
Ben Swanson
Elif Yamangil
Eugene Charniak
Stuart M. Shieber
Spence Green
Sida I. Wang
Daniel M. Cer
Christopher D. Manning
Minwei Feng
Jan-Thorsten Peter
Hermann Ney
Yang Feng
Trevor Cohn
Michael Lucas
Doug Downey
David Bamman
Brendan O'Connor
Noah A. Smith
Sujith Ravi
Stephen Tratz
Eduard H. Hovy
Katsuma Narisawa
Yotaro Watanabe
Junta Mizuno
Naoaki Okazaki
Kentaro Inui
Jackie Chi Kit Cheung
Gerald Penn
Ahmet Aker
Monica Lestari Paramita
Robert J. Gaizauskas
Kashyap Popat
Balamurali A. R.
Pushpak Bhattacharyya
Gholamreza Haffari
Qingqing Cai
Alexander Yates
Muhua Zhu
Yue Zhang
Wenliang Chen
Min Zhang
Jingbo Zhu
Matthew R. Gormley
Jason Eisner
Richard Socher
John Bauer
Christopher D. Manning
Andrew Y. Ng
Angeliki Metallinou
Dan Bohus
Jason Williams
Man Lan
Yu Xu
Zheng-Yu Niu
Shafiq R. Joty
Giuseppe Carenini
Raymond T. Ng
Yashar Mehdad
Emmanuel Lassalle
Pascal Denis
K. Tamsin Maxwell
Jon Oberlander
W. Bruce Croft
Martin Popel
David Marecek
Jan Stepánek
Daniel Zeman
Zdenek Zabokrtský
Flavio De Benedictis
Stefano Faralli
Roberto Navigli
Ulle Endriss
Raquel Fernández
Sebastian Sulger
Miriam Butt
Tracy Holloway King
Paul Meurer
Tibor Laczkó
György Rákosi
Cheikh M. Bamba Dione
Helge Dyvik
Victoria Rosén
Koenraad De Smedt
Agnieszka Patejuk
Özlem Çetinoglu
I Wayan Arka
Meladel Mistica
Olivier Ferret
Carina Silberer
Vittorio Ferrari
Mirella Lapata
Dan Garrette
Jason Mielens
Jason Baldridge
Marion Weller
Alexander M. Fraser
Sabine Schulte im Walde
Haibo Li
Jing Zheng
Heng Ji
Qi Li
Wen Wang
Malte Nuhn
Hermann Ney
Xiaojun Quan
Chunyu Kit
Yan Song
Taesung Lee
Seung-won Hwang
Zhigang Wang
Zhixing Li
Juanzi Li
Jie Tang
Jeff Z. Pan
Vivi Nastase
Carlo Strapparava
Tony Veale
Guofu Li
Arjun Mukherjee
Bing Liu
Zornitsa Kozareva
Tiberiu Boros
Radu Ion
Dan Tufis
Adam Radziszewski
Shane Bergsma
Benjamin Van Durme
Oliver Ferschke
Iryna Gurevych
Marc Rittberger
Aobo Wang
Min-Yen Kan
Oleg Rokhlenko
Idan Szpektor
Dongdong Zhang
Shuangzhi Wu
Nan Yang
Mu Li
Wenbin Jiang
Meng Sun
Yajuan Lü
Yating Yang
Qun Liu
Xiaodong Zeng
Derek F. Wong
Lidia S. Chao
Isabel Trancoso
Trevor Cohn
Gholamreza Haffari
Lemao Liu
Taro Watanabe
Eiichiro Sumita
Tiejun Zhao
Conghui Zhu
Taro Watanabe
Eiichiro Sumita
Tiejun Zhao
Fabienne Braune
Nina Seemann
Daniel Quernheim
Andreas Maletti
Bing Xiang
Xiaoqiang Luo
Bowen Zhou
Rico Sennrich
Holger Schwenk
Walid Aransa
Akihiro Tamura
Taro Watanabe
Eiichiro Sumita
Hiroya Takamura
Manabu Okumura
Guangyou Zhou
Fang Liu
Yang Liu
Shizhu He
Jun Zhao
Roi Reichart
Anna Korhonen
Boyi Xie
Rebecca J. Passonneau
Leon Wu
Germán Creamer
Koichi Tanigaki
Mitsuteru Shiba
Tatsuji Munaka
Yoshinori Sagisaka
Karl Moritz Hermann
Phil Blunsom
Gourab Kundu
Vivek Srikumar
Dan Roth
Asli Çelikyilmaz
Dilek Z. Hakkani-Tür
Gökhan Tür
Ruhi Sarikaya
David Chiang
Jacob Andreas
Daniel Bauer
Karl Moritz Hermann
Bevan Jones
Kevin Knight
Hoifung Poon
Maria Yancheva
Frank Rudzicz
Christian Scheible
Hinrich Schütze
Takayuki Hasegawa
Nobuhiro Kaji
Naoki Yoshinaga
Masashi Toyoda
Verónica Pérez-Rosas
Rada Mihalcea
Louis-Philippe Morency
Mitra Mohtarami
Man Lan
Chew Lim Tan
Vasileios Lampos
Daniel Preotiuc-Pietro
Trevor Cohn
Chen Li
Xian Qian
Yang Liu
Anirban Dasgupta
Ravi Kumar
Sujith Ravi
Hajime Morita
Ryohei Sasano
Hiroya Takamura
Manabu Okumura
Shay B. Cohen
Mark Johnson
Sumire Uematsu
Takuya Matsuzaki
Hiroki Hanaoka
Yusuke Miyao
Hideki Mima
Jinho D. Choi
Andrew McCallum
Kai Liu
Yajuan Lü
Wenbin Jiang
Qun Liu
Mengqiu Wang
Wanxiang Che
Christopher D. Manning
Hongzhao Huang
Zhen Wen
Dian Yu
Heng Ji
Yizhou Sun
Jiawei Han
He Li
Brendan O'Connor
Brandon M. Stewart
Noah A. Smith
Majid Razmara
Maryam Siahbani
Reza Haffari
Anoop Sarkar
Vladimir Eidelman
Yuval Marton
Philip Resnik
Feifei Zhai
Jiajun Zhang
Yu Zhou
Chengqing Zong
Ryo Nagata
Edward W. D. Whittaker
Beata Beigman Klebanov
Michael Flor
Congle Zhang
Tyler Baldwin
Howard Ho
Benny Kimelfeld
Yunyao Li
Zhenhua Tian
Hengheng Xiang
Ziqi Liu
Qinghua Zheng
Egoitz Laparra
German Rigau
Mikhail Kozhevnikov
Ivan Titov
Britta D. Zeller
Jan Snajder
Sebastian Padó
Martin Potthast
Matthias Hagen
Michael Völske
Benno Stein
Tiziano Flati
Roberto Navigli
Jackie Chi Kit Cheung
Gerald Penn
Enrique Alfonseca
Daniele Pighin
Guillermo Garrido
Nina Dethlefs
Helen Wright Hastie
Heriberto Cuayáhuitl
Oliver Lemon
Hendra Setiawan
Bowen Zhou
Bing Xiang
Libin Shen
Karthik Visweswariah
Mitesh M. Khapra
Ananthakrishnan Ramanathan
Boxing Chen
Roland Kuhn
George F. Foster
Tao Lei
Fan Long
Regina Barzilay
Martin C. Rinard
Xiaohua Liu
Yitong Li
Haocheng Wu
Ming Zhou
Furu Wei
Yi Lu
Hua He
Denilson Barbosa
Grzegorz Kondrak
Aline Villavicencio
Marco Idiart
Robert C. Berwick
Igor Malioutov
Oren Melamud
Jonathan Berant
Ido Dagan
Jacob Goldberger
Idan Szpektor
Mohammad Taher Pilehvar
David Jurgens
Roberto Navigli
Francis Bond
Ryan Foster
Silvana Hartmann
Iryna Gurevych
Jason R. Smith
Herve Saint-Amand
Magdalena Plamada
Philipp Koehn
Chris Callison-Burch
Adam Lopez
Lu Wang
Hema Raghavan
Vittorio Castelli
Radu Florian
Claire Cardie
Lu Wang
Claire Cardie
Ravi Kondadadi
Blake Howald
Frank Schilder
Mark Hopkins
Jonathan May
Jiajun Zhang
Chengqing Zong
Marine Carpuat
Hal Daumé III
Katharine Henry
Ann Irvine
Jagadeesh Jagarlamudi
Rachel Rudinger
Gözde Özbal
Daniele Pighin
Carlo Strapparava
Yuanbin Wu
Hwee Tou Ng
Michael Speriosu
Jason Baldridge
Peifeng Li
Qiaoming Zhu
Guodong Zhou
Ndapandula Nakashole
Tomasz Tylenda
Gerhard Weikum
Barbara Plank
Alessandro Moschitti
Benjamin Börschinger
Mark Johnson
Katherine Demuth
Angeliki Lazaridou
Marco Marelli
Roberto Zamparelli
Marco Baroni
Young-Bum Kim
Benjamin Snyder
David Kauchak
Sina Zarrieß
Jonas Kuhn
Kareem Darwish
Malte Nuhn
Julian Schamper
Hermann Ney
Hany Hassan
Arul Menezes
ThuyLinh Nguyen
Stephan Vogel
Yuki Arase
Ming Zhou
Anthony Fader
Luke S. Zettlemoyer
Oren Etzioni
István Varga
Motoki Sano
Kentaro Torisawa
Chikara Hashimoto
Kiyonori Ohtake
Takao Kawai
Jong-Hoon Oh
Stijn De Saeger
Angeliki Lazaridou
Ivan Titov
Caroline Sporleder
Bishan Yang
Claire Cardie
Marta Recasens
Cristian Danescu-Niculescu-Mizil
Dan Jurafsky
Srinivasan Janarthanam
Oliver Lemon
Phil J. Bartie
Tiphaine Dalmas
Anna Dickinson
Xingkun Liu
William A. Mackaness
Bonnie L. Webber
Svitlana Volkova
Pallavi Choudhury
Chris Quirk
Bill Dolan
Luke S. Zettlemoyer
Arjun Mukherjee
Vivek Venkataraman
Bing Liu
Sharon Meraz
Antske Fokkens
Marieke van Erp
Marten Postma
Ted Pedersen
Piek Vossen
Nuno Freire
Chris Fournier
Rohan Ramanath
Monojit Choudhury
Kalika Bali
Rishiraj Saha Roy
Fangtao Li
Yang Gao
Shuchang Zhou
Xiance Si
Decheng Dai
Jong-Hoon Oh
Kentaro Torisawa
Chikara Hashimoto
Motoki Sano
Stijn De Saeger
Kiyonori Ohtake
Wen-tau Yih
Ming-Wei Chang
Christopher Meek
Andrzej Pastusiak
Kang Liu
Liheng Xu
Jun Zhao
Liheng Xu
Kang Liu
Siwei Lai
Yubo Chen
Jun Zhao
Song Feng
Jun Seok Kang
Polina Kuznetsova
Yejin Choi
