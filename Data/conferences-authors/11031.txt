Fifth International Conference on 3D Digital Imaging and Modeling (3DIM 2005), 13-16 June 2005, Ottawa, Ontario, Canada.
Ken-ichi Kanatani
Qian Chen
Toshikazu Wada
Jean-Philippe Tardif
Sébastien Roy
Peter Claes
Dirk Vandermeulen
Luc J. Van Gool
Paul Suetens
Jean-Daniel Deschênes
Patrick Hébert
Philippe Lambert
Jean-Nicolas Ouellet
Dragan Tubic
Marco Fiocco
Gunnar Boström
João G. M. Gonçalves
Vítor Sequeira
Matthew Brown
David G. Lowe
Raghavendra Donamukkala
Daniel F. Huber
Anuj Kapuria
Martial Hebert
Pandu Ranga Rao Devarakota
Bruno Mirbach
Marta Castillo-Franco
Björn E. Ottersten
Xenophon Zabulis
Alexander Patterson
Kostas Daniilidis
Romain Dupont
Renaud Keriven
Philippe Fuchs
Hamid-Reza Pakdel
Faramarz F. Samavati
Xiaoyi Jiang
Da-Chuan Cheng
Byung-Gook Lee
Joon-Jae Lee
Jaechil Yoo
Yuji Kamon
Ryo Yamane
Yasuhiro Mukaigawa
Takeshi Shakunaga
Lyubomir Zagorchev
Ardeshir Goshtasby
Etsuko Ueda
Yoshio Matsumoto
Tsukasa Ogasawara
Gordon Collins
Adrian Hilton
Jason Repko
Marc Pollefeys
Niloofar Gheissari
Alireza Bab-Hadiashar
Yumi Iwashita
Ryo Kurazume
Tsutomu Hasegawa
Kenji Hara
Yonghuai Liu
Longzhuang Li
Baogang Wei
Bradford J. King
Tomasz Malisiewicz
Charles V. Stewart
Richard J. Radke
Helin Dutagaci
Bülent Sankur
Yücel Yemez
Tomoya Terauchi
Yasuhiro Oue
Kouta Fujimura
Zhencheng Hu
Francisco Lamosa
Keiichi Uchimura
Kasper Claes
Thomas P. Koninckx
Herman Bruyninckx
Geert Willems
Frank Verbiest
Maarten Vergauwen
Luc J. Van Gool
Kazunori Umeda
Megumi Shinozaki
Guy Godin
Marc Rioux
Indra Geys
Luc J. Van Gool
Alejandro J. Troccoli
Peter K. Allen
Chen Chao
Ioannis Chao
Nicola Brusco
Marco Andreetto
Andrea Giorgi
Guido Maria Cortelazzo
Ting Li
Pierre Boulanger
Gustavo Osorio
Flavio Prieto
Jean-François Lalonde
Ranjith Unnikrishnan
Nicolas Vandapel
Martial Hebert
Gady Agam
Xiaojing Tang
Ryo Furukawa
Hiroshi Kawasaki
Chris Boehnen
Patrick J. Flynn
Christian Früh
Avideh Zakhor
Li Shen
Takashi Machida
Haruo Takemura
Zouhour Ben Azouz
Chang Shu
Richard Lepage
Marc Rioux
Yu Zhang
Terence Sim
Chew Lim Tan
Pär Hammarstedt
Anders Heyden
Ricardo Oliveira
João Costeira
João Manuel Freitas Xavier
Tomohito Masuda
Yuichiro Hirota
Katsushi Ikeuchi
Ko Nishino
Radim Sára
Ikuko Shimizu Okatani
Akihiro Sugimoto
Peter Claes
Dirk Vandermeulen
Luc J. Van Gool
Paul Suetens
Rashmi Sundareswara
Paul R. Schrater
Guillaume Lavoué
Florent Dupont
Atilla Baskurt
Anders Kaestner
Peter Lehmann
Hannes Fluehler
Gregory G. Slabaugh
Viorel Mihalef
Gozde B. Unal
Jiahui Wang
Hideo Saito
Makoto Kimura
Masaaki Mochimaru
Takeo Kanade
Xiaolan Li
Hongbin Zha
Adrien Theetten
Jean-Philippe Vandeborre
Mohamed Daoudi
Toshihiro Asai
Masayuki Kanbara
Naokazu Yokoya
Kevin Coogan
Isaac Green
Ralph Schönfelder
Joachim Baur
Frank Spenling
Wenfeng He
Wei Ma
Hongbin Zha
Takeshi Oishi
Atsushi Nakazawa
Ryo Kurazume
Katsushi Ikeuchi
Sehwan Kim
Woontack Woo
Hesham Anan
Kurt Maly
Mohammad Zubair
J. B. Briere
M. S. Cordova
E. Galindo
Gabriel Corkidi
Naoyuki Ichimura
Xiang Zhang
Yakup Genc
Limin Shang
Piotr Jasiobedzki
Michael A. Greenspan
Kari Pulli
Simo Piiroinen
Tom Duchamp
Werner Stuetzle
Marc-Antoine Drouin
Martin Trudeau
Sébastien Roy
Minglun Gong
Ruigang Yang
Jangheon Kim
Thomas Sikora
Gabriele Guidi
Laura Loredana Micoli
Michele Russo
Bernard Frischer
Monica De Simone
Alessandro Spinetti
Luca Carosso
Toby P. Breckon
Robert B. Fisher
Peiran Liu
Xiaojun Shen
Nicolas D. Georganas
Gerhard Roth
Xin Liu
Hongxun Yao
Wen Gao
