The 50th Annual Meeting of the Association for Computational Linguistics, Proceedings of the Conference, July 8-14, 2012, Jeju Island, Korea - Volume 2: Short Papers.
Xiao Chen
Chunyu Kit
Reut Tsarfaty
Joakim Nivre
Evelina Andersson
Wanxiang Che
Valentin I. Spitkovsky
Ting Liu
Dave Golland
John DeNero
Jakob Uszkoreit
Nicola Cancedda
Joern Wuebker
Hermann Ney
Richard Zens
Junhui Li
Zhaopeng Tu
Guodong Zhou
Josef van Genabith
Hong Sun
Ming Zhou
Xin Zhao
Rishan Chen
Kai Fan
Hongfei Yan
Xiaoming Li
Seokhwan Kim
Gary Geunbae Lee
Enrique Alfonseca
Katja Filippova
Jean-Yves Delort
Guillermo Garrido
JinYeong Bak
Suin Kim
Alice H. Oh
Pradeep Dasigi
Weiwei Guo
Mona T. Diab
Preethi Raghavan
Albert M. Lai
Eric Fosler-Lussier
Tony Veale
Sravana Reddy
Kevin Knight
Benjamin Börschinger
Mark Johnson
Sida I. Wang
Christopher D. Manning
Sam Sahakian
Benjamin Snyder
Xianchao Wu
Katsuhito Sudoh
Kevin Duh
Hajime Tsukada
Masaaki Nagata
Jonathan K. Kummerfeld
Dan Klein
James R. Curran
Elif Yamangil
Stuart M. Shieber
Vladimir Eidelman
Jordan L. Boyd-Graber
Philip Resnik
Yashar Mehdad
Matteo Negri
Marcello Federico
Lea Frermann
Francis Bond
Joseph Z. Chang
Jason S. Chang
Jyh-Shing Roger Jang
Matt Garley
Julia Hockenmaier
Weiwei Guo
Mona T. Diab
Nikhil Garg
James Henserdon
Igor Labutov
Hod Lipson
Naomi Zeichner
Jonathan Berant
Ido Dagan
Apoorv Agarwal
Adinoyi Omuya
Aaron Harnly
Owen Rambow
Dong Wang
Xian Qian
Yang Liu
Song Feng
Ritwik Banerjee
Yejin Choi
Emad Mohamed
Behrang Mohit
Kemal Oflazer
Luciana Benotti
Martin Villalba
Tessa A. Lau
Julian A. Cerruti
Xiaobing Xue
Yu Tao
Daxin Jiang
Hang Li
Benjamin Swanson
Eugene Charniak
Toshikazu Tajiri
Mamoru Komachi
Yuji Matsumoto
Rafael E. Banchs
Elena Cabrio
Serena Villata
Christian Chiarcos
Kareem Darwish
Ahmed M. Ali
Jennifer Williams
Graham Katz
Joel Nothman
Matthew Honnibal
Ben Hachey
James R. Curran
Yafang Wang
Maximilian Dylla
Marc Spaniol
Gerhard Weikum
Kuzman Ganchev
Keith B. Hall
Ryan T. McDonald
Slav Petrov
Stephen Tyndall
John Lee
Jonathan J. Webster
Nathan Schneider
Behrang Mohit
Kemal Oflazer
Noah A. Smith
Rada Mihalcea
Vivi Nastase
Yanir Seroussi
Fabian Bohnert
Ingrid Zukerman
Pei Yang
Wei Gao
Qi Tan
Kam-Fai Wong
Yuening Hu
Jordan L. Boyd-Graber
Jingbo Zhu
Tong Xiao
Chunliang Zhang
Ning Xi
Guangchao Tang
Xinyu Dai
Shujian Huang
Jiajun Chen
Seung-Wook Lee
Dongdong Zhang
Mu Li
Ming Zhou
Hae-Chang Rim
Andrea Gesmundo
Giorgio Satta
James Henderson
Preslav Nakov
Jörg Tiedemann
Darcey Riley
Daniel Gildea
Isao Goto
Masao Utiyama
Eiichiro Sumita
Hui Zhang
David Chiang
David Stallard
Jacob Devlin
Michael Kayser
Yoong Keok Lee
Regina Barzilay
Hongsuck Seo
Jonghoon Lee
Seokhwan Kim
Kyusong Lee
Sechun Kang
Gary Geunbae Lee
Lei Fang
Minlie Huang
Zhaopeng Tu
Yifan He
Jennifer Foster
Josef van Genabith
Qun Liu
Shouxun Lin
Tsung-Ting Kuo
San-Chuan Hung
Wei-Shih Lin
Nanyun Peng
Shou-De Lin
Wei-Fen Lin
Katsumasa Yoshikawa
Ryu Iida
Tsutomu Hirao
Manabu Okumura
Pierre-Etienne Genest
Guy Lapalme
Karolina Owczarzak
Peter A. Rankel
Hoa Trang Dang
John M. Conroy
Jinho D. Choi
Martha Palmer
Andrea Gesmundo
Tanja Samardzic
Yukino Baba
Hisami Suzuki
Rebecca Dridan
Stephan Oepen
Pierre Magistry
Benoît Sagot
Kenji Imamura
Kuniko Saito
Kugatsu Sadamitsu
Hitoshi Nishikawa
