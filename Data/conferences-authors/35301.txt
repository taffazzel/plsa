The 49th Annual Meeting of the Association for Computational Linguistics: Human Language Technologies, Proceedings of the Conference, 19-24 June, 2011, Portland, Oregon, USA - Short Papers.
Brian Roark
Richard Sproat
Izhak Shafran
Dmitriy Dligach
Martha Palmer
Nikhil Garg
James Henderson
Benjamin Van Durme
Ashwin Lall
Greg Durrett
Dan Klein
Tetsuo Kiso
Masashi Shimbo
Mamoru Komachi
Yuji Matsumoto
Omar Zaidan
Chris Callison-Burch
Kevin Gimpel
Nathan Schneider
Brendan O'Connor
Dipanjan Das
Daniel Mills
Jacob Eisenstein
Michael Heilman
Dani Yogatama
Jeffrey Flanigan
Noah A. Smith
Anders Søgaard
Masato Hagiwara
Satoshi Sekine
Jeffrey Heinz
Chetan Rawal
Herbert G. Tanner
Steffen Hedegaard
Jakob Grue Simonsen
Fei Liu
Fuliang Weng
Bingqing Wang
Yang Liu
Sravana Reddy
Kevin Knight
Manoj Harpalani
Michael Hart
Sandesh Signh
Rob Johnson
Yejin Choi
Chloé Kiddon
Yuriy Brun
Fabrizio Morbini
Kenji Sagae
Richard Johansson
Alessandro Moschitti
Cecilia Ovesdotter Alm
Rivka Levitan
Agustín Gravano
Julia Hirschberg
Anna Margolis
Mari Ostendorf
Micha Elsner
Eugene Charniak
André Bittar
Pascal Amsili
Pascal Denis
Laurence Danlos
Bo Pang
Ravi Kumar
Mehdi Manshadi
James F. Allen
Mary D. Swift
Jagadeesh Jagarlamudi
Hal Daumé III
Raghavendra Udupa
Rafael E. Banchs
Haizhou Li
Maoxi Li
Chengqing Zong
Hwee Tou Ng
Jinxi Xu
Jinying Chen
Hui Lin
Jeff A. Bilmes
Jonathan H. Clark
Chris Dyer
Alon Lavie
Noah A. Smith
Coskun Mermer
Murat Saraclar
Yue Zhang
Joakim Nivre
Daniël de Kok
Barbara Plank
Gertjan van Noord
Colin Cherry
Shane Bergsma
Hiroyuki Shindo
Akinori Fujino
Masaaki Nagata
Shu Cai
David Chiang
Yoav Goldberg
Matt Post
Hajime Morita
Tetsuya Sakai
Manabu Okumura
Anja Belz
Eric Kow
Margaret Mitchell
Aaron Dunlop
Brian Roark
Paul Piwek
Svetlana Stoyanchev
Amjad Abu-Jbara
Barbara Rosario
Kent Lyons
Kapil Thadani
Kathleen McKeown
Shasha Liao
Ralph Grishman
Tara McIntosh
Lars Yencken
James R. Curran
Timothy Baldwin
Oleksandr Kolomiyets
Steven Bethard
Marie-Francine Moens
Truc-Vien T. Nguyen
Alessandro Moschitti
Emilia Apostolova
Noriko Tomuro
Dina Demner-Fushman
Ryan Gabbard
Marjorie Freedman
Ralph M. Weischedel
Qin Gao
Stephan Vogel
Lonneke van der Plas
Paola Merlo
James Henderson
Christian Rohrdantz
Annette Hautli
Thomas Mayer
Miriam Butt
Daniel A. Keim
Frans Plank
Youngjun Kim
Ellen Riloff
Stéphane M. Meystre
Els Lefever
Véronique Hoste
Martine De Cock
Dirk Hovy
Ashish Vaswani
Stephen Tratz
David Chiang
Eduard H. Hovy
Peter LoBue
Alexander Yates
Derya Ozkan
Louis-Philippe Morency
Marjorie Freedman
Alex Baron
Vasin Punyakanok
Ralph M. Weischedel
Alexander Volokh
Günter Neumann
Naushad UzZaman
James F. Allen
Sarah Alkuhlani
Nizar Habash
Clifton James McFate
Kenneth D. Forbus
Saif Mohammad
Wen Wang
Sibel Yaman
Kristin Precoda
Colleen Richey
Geoffrey Raymond
Shujian Huang
Stephan Vogel
Jiajun Chen
Susan Howlett
Mark Dras
Vicent Alabau
Alberto Sanchís
Francisco Casacuberta
Houda Bouamor
Aurélien Max
Anne Vilnat
Licheng Fang
Tagyoung Chung
Daniel Gildea
Hal Daumé III
Jagadeesh Jagarlamudi
Tagyoung Chung
Licheng Fang
Daniel Gildea
Jingbo Zhu
Tong Xiao
Bing Xiang
Abraham Ittycheriah
Kevin Duh
Akinori Fujino
Masaaki Nagata
Takashi Onishi
Masao Utiyama
Eiichiro Sumita
Viet Ha-Thuc
Nicola Cancedda
Sankaranarayanan Ananthakrishnan
Rohit Prasad
Prem Natarajan
Wang Ling
Tiago Luís
João Graça
Isabel Trancoso
Luísa Coheur
David Chiang
Steve DeNeefe
Michael Pust
Kristina Toutanova
Michel Galley
Manaal Faruqui
Sebastian Padó
Bo Li
Éric Gaussier
Akiko N. Aizawa
Ivan Vulic
Wim De Smet
Marie-Francine Moens
Yabin Zheng
Lixing Xie
Zhiyuan Liu
Maosong Sun
Yang Zhang
Liyun Ru
Samuel Brody
Paul B. Kantor
Or Biran
Samuel Brody
Noemie Elhadad
Wenting Xiong
Diane J. Litman
Nitin Madnani
Martin Chodorow
Joel R. Tetreault
Alla Rozovskaya
Delip Rao
David Yarowsky
Zhonghua Qu
Yang Liu
Thomas Müller
Hinrich Schuetze
Graham Neubig
Yosuke Nakata
Shinsuke Mori
Yun Huang
Min Zhang
Chew Lim Tan
Daniel Hewlett
Paul R. Cohen
Donald Metzler
Eduard H. Hovy
Chunliang Zhang
Fumiyo Fukumoto
Yoshimi Suzuki
Eyal Shnarch
Jacob Goldberger
Ido Dagan
Paula Carvalho
Luís Sarmento
Jorge Teixeira
Mário J. Silva
Oscar Täckström
Ryan T. McDonald
Lei Zhang
Bing Liu
Roberto I. González-Ibáñez
Smaranda Muresan
Nina Wacholder
Muhammad Abdul-Mageed
Mona T. Diab
Mohammed Korayem
Ahmed Hassan Awadallah
Amjad Abu-Jbara
Rahul Jha
Dragomir R. Radev
Xipeng Qiu
Xuanjing Huang
Zhao Liu
Jinlong Zhou
Wei-Bin Liang
Chung-Hsien Wu
Chia-Ping Chen
Taniya Mishra
Srinivas Bangalore
Siwei Wang
Gina-Anne Levow
Denis Filimonov
Mary P. Harper
Joel Lang
Nianwen Xue
Yaqin Yang
Jun Suzuki
Hideki Isozaki
Masaaki Nagata
William M. Darling
Fei Song
Xiaojiang Huang
Xiaojun Wan
Jianguo Xiao
Nina Dethlefs
Heriberto Cuayáhuitl
Mariët Theune
Ruud Koolen
Emiel Krahmer
Sander Wubben
William Coster
David Kauchak
Yves Petinot
Kathleen McKeown
Kapil Thadani
Nathan Bodenstab
Kristy Hollingshead
Brian Roark
Anders Søgaard
Jinho D. Choi
Martha Palmer
Seth Kulick
Ann Bies
Justin Mott
Eneko Agirre
Kepa Bengoetxea
Koldo Gojenola
Joakim Nivre
Yoav Goldberg
Michael Elhadad
Gholamreza Haffari
Marzieh Razavi
Anoop Sarkar
Muhua Zhu
Jingbo Zhu
Minghan Hu
Mohit Bansal
Dan Klein
Kugatsu Sadamitsu
Kuniko Saito
Kenji Imamura
Gen-ichiro Kikui
