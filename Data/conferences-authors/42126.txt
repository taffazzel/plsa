Proceedings of the 53rd Annual Meeting of the Association for Computational Linguistics and the 7th International Joint Conference on Natural Language Processing of the Asian Federation of Natural Language Processing, ACL 2015, July 26-31, 2015, Beijing, China, Volume 1: Long Papers.
Sébastien Jean
KyungHyun Cho
Roland Memisevic
Yoshua Bengio
Thang Luong
Ilya Sutskever
Quoc V. Le
Oriol Vinyals
Wojciech Zaremba
Fandong Meng
Zhengdong Lu
Mingxuan Wang
Hang Li
Wenbin Jiang
Qun Liu
Hendra Setiawan
Zhongqiang Huang
Jacob Devlin
Thomas Lamar
Rabih Zbib
Richard M. Schwartz
John Makhoul
Desmond Elliott
Arjen de Vries
Angel X. Chang
Will Monroe
Manolis Savva
Christopher Potts
Christopher D. Manning
Wenpeng Yin
Hinrich Schütze
Arti Ramesh
Shachi H. Kumar
James R. Foulds
Lise Getoor
Shu Guo
Quan Wang
Bin Wang
Lihong Wang
Li Guo
Ignacio Iacobacci
Mohammad Taher Pilehvar
Roberto Navigli
Zhigang Chen
Wei Lin
Qian Chen
Xiaoping Chen
Si Wei
Hui Jiang
Xiaodan Zhu
Dhanya Sridhar
James R. Foulds
Bert Huang
Lise Getoor
Marilyn A. Walker
Audi Primadhanty
Xavier Carreras
Ariadna Quattoni
Fei Sun
Jiafeng Guo
Yanyan Lan
Jun Xu
Xueqi Cheng
Emma Strubell
Luke Vilnis
Kate Silverstein
Andrew McCallum
Arvind Neelakantan
Benjamin Roth
Andrew McCallum
Yubo Chen
Liheng Xu
Kang Liu
Daojian Zeng
Jun Zhao
Vidhoon Viswanathan
Nazneen Fatema Rajani
Yinon Bentor
Raymond J. Mooney
Kiem-Hieu Nguyen
Xavier Tannier
Olivier Ferret
Romaric Besançon
Yusuke Oda
Graham Neubig
Sakriani Sakti
Tomoki Toda
Satoshi Nakamura
Tetsuji Nakagawa
José Guilherme Camargo de Souza
Matteo Negri
Elisa Ricci
Marco Turchi
Jinsong Su
Deyi Xiong
Yang Liu
Xianpei Han
Hongyu Lin
Junfeng Yao
Min Zhang
Mrinmaya Sachan
Kumar Dubey
Eric P. Xing
Matthew Richardson
Guangyou Zhou
Tingting He
Jun Zhao
Po Hu
Li Dong
Furu Wei
Ming Zhou
Ke Xu
Angeliki Lazaridou
Georgiana Dinu
Marco Baroni
Antoine Bride
Tim Van de Cruys
Nicholas Asher
Casey Kennington
David Schlangen
Greg Durrett
Dan Klein
Wenzhe Pei
Tao Ge
Baobao Chang
David Weiss
Chris Alberti
Michael Collins
Slav Petrov
Chris Dyer
Miguel Ballesteros
Wang Ling
Austin Matthews
Noah A. Smith
Gabor Angeli
Melvin Jose Johnson Premkumar
Christopher D. Manning
William Yang Wang
William W. Cohen
Ndapandula Nakashole
Tom M. Mitchell
Maksim Tkachenko
Hady Wirawan Lauw
Jinho D. Choi
Joel R. Tetreault
Amanda Stent
Alan Akbik
Laura Chiticariu
Marina Danilevsky
Yunyao Li
Shivakumar Vaithyanathan
Huaiyu Zhu
Mariana S. C. Almeida
Cláudia Pinto
Helena Figueira
Pedro Mendes
André F. T. Martins
Qiang Chen
Wenjie Li
Yu Lei
Xule Liu
Yanxiang He
Huiwei Zhou
Long Chen
Fulin Shi
Degen Huang
Rahul Jha
Catherine Finegan-Dollak
Ben King
Reed Coke
Dragomir R. Radev
Ondrej Dusek
Filip Jurcícek
Rui Sun
Yue Zhang
Meishan Zhang
Dong-Hong Ji
Young-Bum Kim
Karl Stratos
Ruhi Sarikaya
Minwoo Jeong
Yun-Nung Chen
William Yang Wang
Anatole Gershman
Alexander I. Rudnicky
Shuangzhi Wu
Dongdong Zhang
Ming Zhou
Tiejun Zhao
Yi Yang
Ming-Wei Chang
Chunliang Lu
Wai Lam
Yi Liao
Muyu Zhang
Bing Qin
Mao Zheng
Graeme Hirst
Ting Liu
Jon Scott Stevens
Anton Benz
Sebastian Reuße
Ralf Klabunde
Isaac Persing
Vincent Ng
Ramakrishna Bairi
Rishabh K. Iyer
Ganesh Ramakrishnan
Jeff A. Bilmes
Nikos Voskarides
Edgar Meij
Manos Tsagkias
Maarten de Rijke
Wouter Weerkamp
Tao Ge
Wenzhe Pei
Heng Ji
Sujian Li
Baobao Chang
Zhifang Sui
Boliang Zhang
Hongzhao Huang
Xiaoman Pan
Sujian Li
Chin-Yew Lin
Heng Ji
Kevin Knight
Zhen Wen
Yizhou Sun
Jiawei Han
Bulent Yener
Dirk Weissenborn
Leonhard Hennig
Feiyu Xu
Hans Uszkoreit
Eytan Adar
Srayan Datta
Linlin Wang
Kang Liu
Zhu Cao
Jun Zhao
Gerard de Melo
Cícero Nogueira dos Santos
Bing Xiang
Bowen Zhou
Thien Huu Nguyen
Barbara Plank
Ralph Grishman
Yating Zhang
Adam Jatowt
Sourav S. Bhowmick
Katsumi Tanaka
Bowei Zou
Qiaoming Zhu
Guodong Zhou
Ni Lao
Einat Minkov
William W. Cohen
Yezhou Yang
Yiannis Aloimonos
Cornelia Fermüller
Eren Erdal Aksoy
Guoliang Ji
Shizhu He
Liheng Xu
Kang Liu
Jun Zhao
Christopher Bryant
Hwee Tou Ng
Mihael Arcan
Marco Turchi
Paul Buitelaar
Tristan Miller
Iryna Gurevych
Danushka Bollegala
Takanori Maehara
Ken-ichi Kawarabayashi
José Camacho-Collados
Mohammad Taher Pilehvar
Roberto Navigli
Dirk Hovy
Asad B. Sayeed
Stefan Fischer
Vera Demberg
Doug Downey
Chandra Bhagavatula
Yi Yang
Bin Wang
Zhijian Ou
Zhiqiang Tan
Rajarshi Das
Manzil Zaheer
Chris Dyer
Francisco Guzmán
Shafiq R. Joty
Lluís Màrquez
Preslav Nakov
Nina Seemann
Fabienne Braune
Andreas Maletti
Shujian Huang
Huadong Chen
Xinyu Dai
Jiajun Chen
Qing Dou
Ashish Vaswani
Kevin Knight
Chris Dyer
Antonio Valerio Miceli Barone
Giuseppe Attardi
Dian Yu
Yulia Tyshchuk
Heng Ji
William A. Wallace
Siddhartha Banerjee
Prasenjit Mitra
Chris Quirk
Raymond J. Mooney
Michel Galley
Igor Labutov
Sumit Basu
Lucy Vanderwende
Nguyen Ha Vo
Arindam Mitra
Chitta Baral
Steffen Eger
Ke Xu
Yunqing Xia
Chin-Hui Lee
Chen Li
Yang Liu
Martin Gleize
Brigitte Grau
Ekaterina Shutova
Niket Tandon
Gerard de Melo
Hiroki Ouchi
Hiroyuki Shindo
Kevin Duh
Yuji Matsumoto
Nghia The Pham
Germán Kruszewski
Angeliki Lazaridou
Marco Baroni
Keenon Werling
Gabor Angeli
Christopher D. Manning
Dipendra Kumar Misra
Kejia Tao
Percy Liang
Ashutosh Saxena
Simone Filice
Giovanni Da San Martino
Alessandro Moschitti
Duyu Tang
Bing Qin
Ting Liu
Andrew Schneider
Eduard C. Dragut
Byron C. Wallace
Do Kook Choe
Eugene Charniak
Shoushan Li
Lei Huang
Rong Wang
Guodong Zhou
Rui Xia
Cheng Wang
Xin-Yu Dai
Tao Li
Caroline Langlet
Chloé Clavel
Ramón Fernandez Astudillo
Silvio Amir
Wang Ling
Mário Silva
Isabel Trancoso
Vinay Shashidhar
Nishant Pandey
Varun Aggarwal
Shahab Jalalvand
Matteo Negri
Daniele Falavigna
Marco Turchi
Jiwei Li
Minh-Thang Luong
Dan Jurafsky
Alexis Nasr
Carlos Ramisch
José Deulofeu
André Valli
Jie Zhou
Wei Xu
Zhiguo Wang
Haitao Mi
Nianwen Xue
Dominick Ng
James R. Curran
Chenxi Zhu
Xipeng Qiu
Xinchi Chen
Xuanjing Huang
Taro Watanabe
Eiichiro Sumita
Xian Qian
Yang Liu
Ioannis Konstas
Frank Keller
Wolfgang Maier
Hao Zhou
Yue Zhang
Shujian Huang
Jiajun Chen
Do Kook Choe
David McClosky
Jiang Guo
Wanxiang Che
David Yarowsky
Haifeng Wang
Ting Liu
Marti A. Hearst
Karthik Narasimhan
Regina Barzilay
Ru Li
Juan Wu
Zhiqiang Wang
Qinghua Chai
Annemarie Friedrich
Manfred Pinkal
Karl Stratos
Michael Collins
Daniel J. Hsu
Zhiting Hu
Poyao Huang
Yuntian Deng
Yingkai Gao
Eric P. Xing
Jeff Mitchell
Mark Steedman
Eunsol Choi
Tom Kwiatkowski
Luke Zettlemoyer
Wen-tau Yih
Ming-Wei Chang
Xiaodong He
Jianfeng Gao
Yushi Wang
Jonathan Berant
Percy Liang
Xin Wang
Yuanchao Liu
Chengjie Sun
Baoxun Wang
Xiaolong Wang
Thien Hai Nguyen
Kiyoaki Shirai
Qiao Qian
Bo Tian
Minlie Huang
Yang Liu
Xuan Zhu
Xiaoyan Zhu
Edouard Grave
Noémie Elhadad
Jason Mielens
Liang Sun
Jason Baldridge
Yonatan Bisk
Julia Hockenmaier
Kevin Clark
Christopher D. Manning
Sam Wiseman
Alexander M. Rush
Stuart M. Shieber
Jason Weston
André F. T. Martins
Viet-An Nguyen
Jordan L. Boyd-Graber
Philip Resnik
Kristina Miler
Dana Movshovitz-Attias
William W. Cohen
Zhendong Zhao
Lan Du
Benjamin Börschinger
John K. Pate
Massimiliano Ciaramita
Mark Steedman
Mark Johnson
Panupong Pasupat
Percy Liang
Jonas Groschwitz
Alexander Koller
Christoph Teichmann
Manaal Faruqui
Yulia Tsvetkov
Dani Yogatama
Chris Dyer
Noah A. Smith
Quan Liu
Hui Jiang
Si Wei
Zhen-Hua Ling
Yu Hu
Ellie Pavlick
Johan Bos
Malvina Nissim
Charley Beller
Benjamin Van Durme
Chris Callison-Burch
Daniel Fernández-González
André F. T. Martins
Le Quang Thang
Hiroshi Noji
Yusuke Miyao
Yantao Du
Weiwei Sun
Xiaojun Wan
Kai Sheng Tai
Richard Socher
Christopher D. Manning
Mingxuan Wang
Zhengdong Lu
Hang Li
Wenbin Jiang
Qun Liu
Lifeng Shang
Zhengdong Lu
Hang Li
Lidong Bing
Piji Li
Yi Liao
Wai Lam
Weiwei Guo
Rebecca J. Passonneau
Giang Binh Tran
Eelco Herder
Katja Markert
Chris Kedzie
Kathleen McKeown
Fernando Diaz
Jey Han Lau
Alexander Clark
Shalom Lappin
Oren Tsur
Dan Calacci
David Lazer
Naho Orita
Eliana Vornov
Naomi Feldman
Hal Daumé III
Vlad Niculae
Srijan Kumar
Jordan L. Boyd-Graber
Cristian Danescu-Niculescu-Mizil
Shin Kanouchi
Mamoru Komachi
Naoaki Okazaki
Eiji Aramaki
Hiroshi Ishikawa
Diyi Yang
Miaomiao Wen
Carolyn Penstein Rosé
Mohit Iyyer
Varun Manjunatha
Jordan L. Boyd-Graber
Hal Daumé III
Jialei Wang
Ji Wan
Yongdong Zhang
Steven C. H. Hoi
François Rousseau
Emmanouil Kiagias
Michalis Vazirgiannis
Anders Søgaard
Zeljko Agic
Héctor Martínez Alonso
Barbara Plank
Bernd Bohnet
Anders Johannsen
Daxiang Dong
Hua Wu
Wei He
Dianhai Yu
Haifeng Wang
Jianqiang Ma
Erhard W. Hinrichs
Xinchi Chen
Xipeng Qiu
Chenxi Zhu
Xuanjing Huang
Daniel Preotiuc-Pietro
Vasileios Lampos
Nikolaos Aletras
Dominik Wurzer
Victor Lavrenko
Miles Osborne
Kei Uchiumi
Hiroshi Tsukahara
Daichi Mochihashi
Zhenghua Li
Jiayuan Chao
Min Zhang
Wenliang Chen
Sascha Rothe
Hinrich Schütze
Yvette Graham
