Engineering Self-Organising Systems, Nature-Inspired Approaches to Software Engineering [revised and extended papers presented at the Engineering Self-Organising Applications Workshop, ESOA 2003, held at AAMAS 2003 in Melbourne, Australia, in July 2003 and selected invited papers from leading researchers in self-organisation].
Franco Zambonelli
Andrea Omicini
Bernhard Anzengruber
Gabriella Castelli
Francesco L. De Angelis
Giovanna Di Marzo Serugendo
Simon A. Dobson
Jose Luis Fernandez-Marquez
Alois Ferscha
Marco Mamei
Stefano Mariani
Ambra Molesini
Sara Montagna
Jussi Nieminen
Danilo Pianini
Matteo Risoldi
Alberto Rosi
Graeme Stevenson
Mirko Viroli
Juan Ye
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Gabriella Castelli
Francesco Luca De Angelis
Giovanna Di Marzo Serugendo
Francesco Luca De Angelis
Giovanna Di Marzo Serugendo
Mohammad Parhizkar
Giovanna Di Marzo Serugendo
Francesco Luca De Angelis
Giovanna Di Marzo Serugendo
Regina Frei
Traian-Florin Serbanuta
Giovanna Di Marzo Serugendo
Pedro Ferrera
Ivan de Prado
Eric Palacios
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Graeme Stevenson
Juan Ye
Simon Dobson
Franco Zambonelli
Francesco De Angelis
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Francesco De Angelis
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Jose Luis Fernandez-Marquez
Francesco De Angelis
Giovanna Di Marzo Serugendo
Graeme Stevenson
Gabriella Castelli
Sara Montagna
Mirko Viroli
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Franco Zambonelli
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Sara Montagna
Mirko Viroli
Josep Lluís Arcos
Francesco De Angelis
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Giovanna Di Marzo Serugendo
Jose Luis Fernandez-Marquez
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Jose Luis Fernandez-Marquez
Josep Lluís Arcos
Giovanna Di Marzo Serugendo
Mariachiara Puviani
Giovanna Di Marzo Serugendo
Regina Frei
Giacomo Cabri
Pedro Ferrera
Ivan de Prado
Eric Palacios
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Akla-Esso Tchao
Giovanna Di Marzo Serugendo
Antonio Coronato
Vincenzo De Florio
Mohamed Bakhouya
Giovanna Di Marzo Serugendo
Paul L. Snyder
Giuseppe Valetto
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Jose Luis Fernandez-Marquez
Akla-Esso Tchao
Giovanna Di Marzo Serugendo
Graeme Stevenson
Juan Ye
Simon Dobson
Graeme Stevenson
Jose Luis Fernandez-Marquez
Sara Montagna
Alberto Rosi
Juan Ye
Akla-Esso Tchao
Simon Dobson
Giovanna Di Marzo Serugendo
Mirko Viroli
Vincenzo De Florio
Antonio Coronato
Mohamed Bakhouya
Giovanna Di Marzo Serugendo
Regina Frei
Giovanna Di Marzo Serugendo
Regina Frei
Giovanna Di Marzo Serugendo
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Josep Lluís Arcos
Regina Frei
Giovanna Di Marzo Serugendo
Jose Luis Fernandez-Marquez
Giovanna Di Marzo Serugendo
Sara Montagna
Akla-Esso Tchao
Matteo Risoldi
Giovanna Di Marzo Serugendo
Sara Montagna
Mirko Viroli
Matteo Risoldi
Danilo Pianini
Giovanna Di Marzo Serugendo
Franco Zambonelli
Gabriella Castelli
Laura Ferrari
Marco Mamei
Alberto Rosi
Giovanna Di Marzo Serugendo
Matteo Risoldi
Akla-Esso Tchao
Simon Dobson
Graeme Stevenson
Juan Ye
Elena Nardini
Andrea Omicini
Sara Montagna
Mirko Viroli
Alois Ferscha
Sascha Maschek
Bernhard Wally
Regina Frei
Giovanna Di Marzo Serugendo
Traian-Florin Serbanuta
Giovanna Di Marzo Serugendo
John S. Fitzgerald
Alexander Romanovsky
Jose Luis Fernandez-Marquez
Josep Lluís Arcos
Giovanna Di Marzo Serugendo
Giovanna Di Marzo Serugendo
Regina Frei
Jose Luis Fernandez-Marquez
Josep Lluís Arcos
Giovanna Di Marzo Serugendo
Alfredo A. Villalba Castro
Giovanna Di Marzo Serugendo
Dimitri Konstantas
Betty H. C. Cheng
Rogério de Lemos
Holger Giese
Paola Inverardi
Jeff Magee
Jesper Andersson
Basil Becker
Nelly Bencomo
Yuriy Brun
Bojan Cukic
Giovanna Di Marzo Serugendo
Schahram Dustdar
Anthony Finkelstein
Cristina Gacek
Kurt Geihs
Vincenzo Grassi
Gabor Karsai
Holger M. Kienle
Jeff Kramer
Marin Litoiu
Sam Malek
Raffaela Mirandola
Hausi A. Müller
Sooyong Park
Mary Shaw
Matthias Tichy
Massimo Tivoli
Danny Weyns
Jon Whittle
Yuriy Brun
Giovanna Di Marzo Serugendo
Cristina Gacek
Holger Giese
Holger M. Kienle
Marin Litoiu
Hausi A. Müller
Mauro Pezzè
Mary Shaw
Mariachiara Puviani
Giovanna Di Marzo Serugendo
Regina Frei
Giacomo Cabri
Regina Frei
Bruno Ferreira
Giovanna Di Marzo Serugendo
José Barata
Giovanna Di Marzo Serugendo
Giovanna Di Marzo Serugendo
Alfredo A. Villalba Castro
Dimitri Konstantas
Liu Liang
Duan Zheng-yu
G. S. Thyagaraju
Umakanth P. Kulkarni
Anil R. Yardi
Gonzalo Huerta Cánepa
Angel Jiménez Molina
In-Young Ko
Dongman Lee
Jaewook Jung
Youngjae Kim
Minsoo Hahn
Tatsuya Yamazaki
Tetsuo Toyomura
Takashi Matsuyama
D. B. Kulkarni
Regina Frei
Giovanna Di Marzo Serugendo
José Barata
Alfredo A. Villalba Castro
Giovanna Di Marzo Serugendo
Dimitri Konstantas
Mukesh Singhal
Giovanna Di Marzo Serugendo
Jeffrey J. P. Tsai
Wang-Chien Lee
Kay Römer
Yu-Chee Tseng
Han C. W. Hsiao
Giovanna Di Marzo Serugendo
John S. Fitzgerald
Alexander Romanovsky
Nicolas Guelfi
Martin Kelly
Giovanna Di Marzo Serugendo
Arne-Jørgen Berre
Giovanna Di Marzo Serugendo
Djamel Khadraoui
François Charoy
George Athanasopoulos
Michael Pantazoglou
Jean-Henry Morin
Pavlos Moraitis
Nikolaos I. Spanoudakis
Giovanna Di Marzo Serugendo
John S. Fitzgerald
Alexander Romanovsky
Nicolas Guelfi
Frank Ortmeier
Wolfgang Reif
Giovanna Di Marzo Serugendo
Hartmut Schmeck
Giovanna Di Marzo Serugendo
Marie Pierre Gleizes
Anthony Karageorgos
Salima Hassas
Giovanna Di Marzo Serugendo
Anthony Karageorgos
Cristiano Castelfranchi
Giovanna Di Marzo Serugendo
Martin Kelly
Giovanna Di Marzo Serugendo
Giovanna Di Marzo Serugendo
Sven Brueckner
Giovanna Di Marzo Serugendo
David Hales
Franco Zambonelli
Giovanna Di Marzo Serugendo
Marie Pierre Gleizes
Anthony Karageorgos
Giovanna Di Marzo Serugendo
Sven Brueckner
Giovanna Di Marzo Serugendo
Anthony Karageorgos
Radhika Nagpal
Manuel Oriol
Giovanna Di Marzo Serugendo
Jarle Hulaas
Walter Binder
Giovanna Di Marzo Serugendo
Giovanna Di Marzo Serugendo
Anthony Karageorgos
Omer F. Rana
Franco Zambonelli
Vinny Cahill
Elizabeth Gray
Jean-Marc Seigneur
Christian Damsgaard Jensen
Yong Chen
Brian Shand
Nathan Dimmock
Andrew Twigg
Jean Bacon
Colin English
Waleed Wagealla
Sotirios Terzis
Paddy Nixon
Giovanna Di Marzo Serugendo
Ciarán Bryce
Marco Carbone
Karl Krukow
Mogens Nielsen
Giovanna Di Marzo Serugendo
Noria Foukia
Salima Hassas
Anthony Karageorgos
Soraya Kouadri Mostéfaoui
Omer F. Rana
Mihaela Ulieru
Paul Valckenaers
Chris van Aart
Giovanna Di Marzo Serugendo
Giovanna Di Marzo Serugendo
Dino Mandrioli
Didier Buchs
Nicolas Guelfi
Giovanna Di Marzo Serugendo
Alexander B. Romanovsky
Giovanna Di Marzo Serugendo
Giovanna Di Marzo Serugendo
Nicolas Guelfi
Alexander B. Romanovsky
Avelino F. Zorzo
Giovanna Di Marzo Serugendo
Murhimanya Muhugusa
Christian F. Tschudin
Giovanna Di Marzo Serugendo
Nicolas Guelfi
Giovanna Di Marzo Serugendo
Nicolas Guelfi
